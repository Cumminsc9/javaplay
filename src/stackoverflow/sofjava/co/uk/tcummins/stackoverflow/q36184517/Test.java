package co.uk.tcummins.stackoverflow.q36184517;

import java.lang.invoke.CallSite;
import java.lang.invoke.LambdaMetafactory;
import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodHandles.Lookup;
import java.lang.invoke.MethodType;
import java.lang.reflect.Field;


/**
 * Created by tcummins on 23.03.16.
 */
public class Test
{
    /**
     * [CZ = char[]
     * Z = boolean type
     * V = void
     *
     * @param args
     * @throws Throwable
     */
    public static void main( String[] args ) throws Throwable
    {
        // Reflectively generate a TRUSTED Lookup for the calling class
        Lookup caller = MethodHandles.lookup();
        Field modes = Lookup.class.getDeclaredField( "allowedModes" );
        modes.setAccessible( true );

        // -1 == Lookup.TRUSTED
        modes.setInt( caller, -1 );

        // create handle for #create()
        MethodHandle conventional = caller.findStatic( Test.class, "create", MethodType.methodType( String.class, char[].class, boolean.class ) );
        StringCreator normal = getStringCreator( caller, conventional );

        // prints "foo"
        System.out.println( normal.create( "foo".toCharArray(), true ) );

        // create handle for shared String constructor // test directly if the construcor is correctly accessed
        MethodHandle constructor = caller.findConstructor( String.class, MethodType.methodType( void.class, char[].class, boolean.class ) );

        char[] chars = "foo".toCharArray();
        String s = (String) constructor.invokeExact( chars, true );

        // modify array contents
        chars[0] = 'b';
        chars[1] = 'a';
        chars[2] = 'r';

        // prints "bar"
        System.out.println( s );

        // generate interface for constructor
        StringCreator shared = getStringCreator( caller, constructor );

        // throws error
        System.out.println( shared.create( "foo".toCharArray(), true ) );
    }


    // returns a StringCreator instance
    private static StringCreator getStringCreator( Lookup caller, MethodHandle handle ) throws Throwable
    {
        CallSite callSite = LambdaMetafactory.metafactory( caller, "create", MethodType.methodType( StringCreator.class ), handle.type(), handle, handle.type() );

        char[] c = {};
        boolean b = false;

        return (StringCreator) callSite.getTarget().invokeExact( );
    }



    // Creates a new conventional String
    private static String create( char[] value, boolean shared )
    {
        return String.valueOf( value );
    }
}