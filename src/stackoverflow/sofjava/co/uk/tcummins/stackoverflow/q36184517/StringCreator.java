package co.uk.tcummins.stackoverflow.q36184517;

/**
 * Created by tcummins on 23.03.16.
 */
public interface StringCreator
{
    String create( char[] value, boolean shared );
}