package co.uk.tcummins.stackoverflow.q35872407;

/**
 * Created by tcummins on 08.03.16. Project : JavaPlay
 */
public class MyPojo
{
    private MyEnum myEnum;


    public MyEnum getMyEnum()
    {
        return myEnum;
    }


    public void setMyEnum( final MyEnum myEnum )
    {
        this.myEnum = myEnum;
    }
}
