package co.uk.tcummins.stackoverflow.q35872407;

/**
 * Created by tcummins on 08.03.16.
 * Project : JavaPlay
 */
public class MyClass
{
    public static void main( String[] args )
    {
        final MyPojo pojo = new MyPojo();

        switch ( pojo.getMyEnum() )
        {
            case VALUE_1:
                break;
            case VALUE_2:
                break;
            default:
                break;
        }
    }
}
