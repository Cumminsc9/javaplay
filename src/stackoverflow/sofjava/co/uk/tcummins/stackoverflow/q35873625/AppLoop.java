package co.uk.tcummins.stackoverflow.q35873625;

import java.util.Scanner;


/**
 * Created by tcummins on 15.03.16.
 * Project : JavaPlay
 */
public class AppLoop
{
    public static void main( String[] args )
    {
        Scanner inputLine = new Scanner( System.in );
        String staffName, quit;
        double hoursWorked = 0, hourlyRate = 15, totalPay = 0, nationalInsurance = 0, tax = 0, netPay = 0, overtime = 0;

        do
        {
            System.out.print( "Enter employee name: " );
            staffName = inputLine.next();

            System.out.print( "Enter number of hours worked: " );
            hoursWorked = inputLine.nextFloat();

            if ( hoursWorked <= 36 )
            {
                totalPay = ( hourlyRate * hoursWorked );
            }
            else if ( hoursWorked >= 37 && hoursWorked <= 40 )
            {
                totalPay = ( hourlyRate * 36 ) + ( hoursWorked - 36 ) * ( hourlyRate * 1.5 );
            }
            else
            {
                totalPay = ( hourlyRate * 36 ) + ( 41 - 36 ) * ( hourlyRate * 1.5 ) +
                           ( hoursWorked - 41 ) * ( hourlyRate * 2 );
            }

            if ( hoursWorked >= 37 && hoursWorked <= 40 )
            {
                overtime = ( hoursWorked - 36 ) * ( hourlyRate * 1.5 );
            }
            else if ( hoursWorked > 41 )
            {
                overtime = ( 41 - 36 ) * ( hourlyRate * 1.5 ) + ( hoursWorked - 41 ) * ( hourlyRate * 2 );
            }

            if ( totalPay > 155 )
            {
                nationalInsurance = ( totalPay * 0.12 );
            }

            tax = ( totalPay * 0.20 );
            netPay = ( totalPay - tax - nationalInsurance );

            if ( hoursWorked >= 49 )
            {
                System.out.println( "You are not legally allowed to work over 48 hours! " );
            }
            else
            {
                System.out.println( "***********************" );
                System.out.println( "Employee: " + staffName );
                System.out.println( "Total Hours Worked: " + hoursWorked );
                System.out.println( "Overtime Pay: " + overtime );
                System.out.println( "Net Pay: " + totalPay );
                System.out.println( "Tax: " + tax );
                System.out.println( "National insurance: " +
                                    ( nationalInsurance = Math.round( nationalInsurance * 100.00 ) / 100.00 ) );
                System.out.println( "Net Pay" + netPay );
            }

            System.out.println( "Would you like to create another account? Y/N: " );
            quit = inputLine.next();
        }
        while ( !quit.equalsIgnoreCase( "N" ) );
    }
}

