package co.uk.tcummins.stackoverflow.tests;

public class Obj
{
    private String str1;
    private String str2;
    private String str3;


    public Obj()
    {
    }


    public Obj( final String str1, final String str2, final String str3 )
    {
        this.str1 = str1;
        this.str2 = str2;
        this.str3 = str3;
    }


    public String getStr1()
    {
        return str1;
    }


    public void setStr1( final String str1 )
    {
        this.str1 = str1;
    }


    public String getStr2()
    {
        return str2;
    }


    public void setStr2( final String str2 )
    {
        this.str2 = str2;
    }


    public String getStr3()
    {
        return str3;
    }


    public void setStr3( final String str3 )
    {
        this.str3 = str3;
    }


    @Override
    public String toString()
    {
        return super.toString();
    }
}