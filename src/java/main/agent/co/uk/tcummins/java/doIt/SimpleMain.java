package co.uk.tcummins.java.doIt;

import java.lang.instrument.Instrumentation;


/**
 * Created by tcummins on 20.06.16
 */
public class SimpleMain
{
    public static void premain( String agentArguments, Instrumentation instrumentation )
    {
        instrumentation.addTransformer( new SimpleTransformer() );
    }
}
