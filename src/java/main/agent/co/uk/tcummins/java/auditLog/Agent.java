package co.uk.tcummins.java.auditLog;

import java.lang.instrument.Instrumentation;


/**
 * Created by tcummins on 28.06.16
 */
public class Agent
{
    /**
     * Manifest: Premain-Class: co.uk.tcummins.java.auditLog.Agent
     */
    public static void premain( String agentArgs, Instrumentation inst )
    {
        System.out.println( "Starting the agent" );
        inst.addTransformer( new ImportantLogClassTransformer() );
    }
}