package co.uk.tcummins.java.calculateMemory;

import java.lang.instrument.Instrumentation;


/**
 * Created by tcummins on 28.06.16
 */
public class Agent
{
    private static volatile Instrumentation globalInstrumentation;

    public static void premain( String agentArgs, Instrumentation inst )
    {
        globalInstrumentation = inst;
    }

    private static long getObjectSize( Object obj )
    {
        if( globalInstrumentation == null )
        {
            throw new IllegalStateException( "Agent not initted" );
        }

        return globalInstrumentation.getObjectSize( obj );
    }
}
