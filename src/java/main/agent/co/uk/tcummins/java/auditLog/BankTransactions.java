package co.uk.tcummins.java.auditLog;

/**
 * Created by tcummins on 28.06.16
 * https://blog.newrelic.com/2014/09/29/diving-bytecode-manipulation-creating-audit-log-asm-
 * javassist/
 */
public class BankTransactions
{
    public static void main( String[] args )
    {
        BankTransactions bank = new BankTransactions();

        for( int i = 0; i < 100; i++ )
        {
            String accountID = "account" + i;
            bank.login( "password", accountID, "Tom" );
            bank.unimportantProcessing( accountID );
            bank.withdraw( accountID, (double) i );
        }

        System.out.println( "Transactions Complete.." );
    }


    @ImportantLog(fields = { "1", "2" })
    private void login( String password, String accountID, String userName )
    {
        // login logic
    }


    private void unimportantProcessing( String accountID )
    {
        // unimportantProcessing logic
    }


    @ImportantLog(fields = { "1", "2" })
    public void withdraw( String accountID, double moneyToRemove )
    {
        // withdraw logic
    }
}