package co.uk.tcummins.java.doIt;

/**
 * Created by tcummins on 20.06.16
 */
public class DoItClass
{
    public static void main( String[] args )
    {
        DoItClass doItClass = new DoItClass();
        doItClass.doIt();
    }

    private void doIt()
    {
        try
        {
            System.out.println( "Ran doIt() method" );
            Thread.sleep( 5000 );
        }
        catch ( Exception e )
        {
            e.printStackTrace();
        }
    }
}