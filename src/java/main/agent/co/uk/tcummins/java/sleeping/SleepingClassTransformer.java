package co.uk.tcummins.java.sleeping;

import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;

import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.security.ProtectionDomain;


/**
 * Created by tcummins on 20.06.16
 */
public class SleepingClassTransformer implements ClassFileTransformer
{
    public byte[] transform( final ClassLoader loader, final String className, final Class<?> classBeingRedefined,
                             final ProtectionDomain protectionDomain, final byte[] classfileBuffer ) throws IllegalClassFormatException
    {
        byte[] byteCode = classfileBuffer;

        if( className.equals( "co/uk/tcummins/java/sleeping/Sleeping" ) )
        {
            CtClass ctClass = null;
            try
            {
                ClassPool classPool = ClassPool.getDefault();
                ctClass = classPool.get( "co.uk.tcummins.java.sleeping.Sleeping" );

                CtMethod ctMethod = ctClass.getDeclaredMethod( "randomSleep" );
                ctMethod.addLocalVariable( "elapsedTime", CtClass.longType );
                ctMethod.insertBefore("elapsedTime = System.currentTimeMillis();");
                ctMethod.insertAfter("{elapsedTime = System.currentTimeMillis() - elapsedTime;" + "System.out.println(\"Method Executed in ms: \" + elapsedTime);}");

                byteCode = ctClass.toBytecode();
            }
            catch ( Exception e )
            {
                e.printStackTrace();
            }
            finally
            {
                if ( ctClass != null )
                {
                    ctClass.detach();
                }
            }
        }

        return byteCode;
    }
}