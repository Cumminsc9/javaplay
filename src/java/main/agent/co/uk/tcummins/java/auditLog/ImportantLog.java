package co.uk.tcummins.java.auditLog;

/**
 * Created by tcummins on 28.06.16
 */
public @interface ImportantLog
{
    /**
     * The method parameter indexes whose values should be logged. For example, if we have the method;
     * hello(int paramA, int paramB, int paramC), and we wanted to log the values of paramA and paramC,
     * then fields would ["0", "2"] If we only want to log the value of paramB, then the fields would be ["1"]
     */
    String[] fields();
}
