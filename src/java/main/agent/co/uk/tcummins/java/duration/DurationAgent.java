package co.uk.tcummins.java.duration;

import java.lang.instrument.Instrumentation;


/**
 * Created by tcummins on 20.06.16
 */
public class DurationAgent
{
    public static void premain( String agentArgs, Instrumentation inst )
    {
        System.out.println( "Executing premain." );
        inst.addTransformer( new DurationTransformer() );
    }
}