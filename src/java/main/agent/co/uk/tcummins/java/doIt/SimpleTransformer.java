package co.uk.tcummins.java.doIt;

import javassist.ClassPool;
import javassist.CtBehavior;
import javassist.CtClass;

import java.io.IOException;
import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.security.ProtectionDomain;


/**
 * Created by tcummins on 20.06.16
 */
public class SimpleTransformer implements ClassFileTransformer
{
    public SimpleTransformer()
    {
        super();
    }


    @Override
    public byte[] transform( final ClassLoader loader, final String className, final Class<?> classBeingRedefined,
                             final ProtectionDomain protectionDomain, final byte[] classfileBuffer ) throws
                                                                                                     IllegalClassFormatException
    {
        return transformClass( classBeingRedefined, classfileBuffer );
    }


    private byte[] transformClass( final Class<?> classBeingRedefined, byte[] classfileBuffer )
    {
        ClassPool pool = ClassPool.getDefault();
        CtClass cl = null;

        try
        {
            cl = pool.makeClass( new java.io.ByteArrayInputStream( classfileBuffer ) );
            CtBehavior[] methods = cl.getDeclaredBehaviors();
            for ( CtBehavior method : methods )
            {
                if ( !method.isEmpty() )
                {
                    changeMethod( method );
                }
            }
            classfileBuffer = cl.toBytecode();
        }
        catch ( Exception e )
        {
            e.printStackTrace();
        }
        finally
        {
            if( cl != null )
            {
                cl.detach();
            }
        }

        return classfileBuffer;
    }


    private void changeMethod( final CtBehavior method ) throws Exception
    {
        if( method.getName().equals( "doIt" ) )
        {
            method.insertBefore("System.out.println(\"started method at \" + new java.util.Date());");
            method.insertAfter("System.out.println(\"ended method at \" + new java.util.Date());");
        }
    }
}