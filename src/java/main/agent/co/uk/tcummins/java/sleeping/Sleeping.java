package co.uk.tcummins.java.sleeping;

/**
 * Created by tcummins on 20.06.16
 */
public class Sleeping
{
    public void randomSleep() throws Exception
    {
        // Randomly sleeps between 500ms and 1200ms
        long randomSleepDuration = (long) (500 + Math.random() * 700 );
        System.out.printf( "Sleeping for %d ms ...\n", randomSleepDuration );
        Thread.sleep( randomSleepDuration );
    }
}