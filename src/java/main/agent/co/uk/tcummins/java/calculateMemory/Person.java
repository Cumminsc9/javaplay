package co.uk.tcummins.java.calculateMemory;

/**
 * Created by tcummins on 28.06.16
 */
public class Person
{
    private String firstName;
    private String secondName;
    private String addressLineOne;
    private String addressLineTwo;
    private String postcode;
    private String county;
    private int phoneNumber;

    public Person()
    {
    }


    public Person( final String firstName, final String secondName, final String addressLineOne,
                   final String addressLineTwo, final String postcode, final String county, final int phoneNumber )
    {
        this.firstName = firstName;
        this.secondName = secondName;
        this.addressLineOne = addressLineOne;
        this.addressLineTwo = addressLineTwo;
        this.postcode = postcode;
        this.county = county;
        this.phoneNumber = phoneNumber;
    }


    public String getFirstName()
    {
        return firstName;
    }


    public void setFirstName( final String firstName )
    {
        this.firstName = firstName;
    }


    public String getSecondName()
    {
        return secondName;
    }


    public void setSecondName( final String secondName )
    {
        this.secondName = secondName;
    }


    public String getAddressLineOne()
    {
        return addressLineOne;
    }


    public void setAddressLineOne( final String addressLineOne )
    {
        this.addressLineOne = addressLineOne;
    }


    public String getAddressLineTwo()
    {
        return addressLineTwo;
    }


    public void setAddressLineTwo( final String addressLineTwo )
    {
        this.addressLineTwo = addressLineTwo;
    }


    public String getPostcode()
    {
        return postcode;
    }


    public void setPostcode( final String postcode )
    {
        this.postcode = postcode;
    }


    public String getCounty()
    {
        return county;
    }


    public void setCounty( final String county )
    {
        this.county = county;
    }


    public int getPhoneNumber()
    {
        return phoneNumber;
    }


    public void setPhoneNumber( final int phoneNumber )
    {
        this.phoneNumber = phoneNumber;
    }
}
