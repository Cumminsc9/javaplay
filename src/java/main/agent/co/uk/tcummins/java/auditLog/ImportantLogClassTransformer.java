package co.uk.tcummins.java.auditLog;

import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.security.ProtectionDomain;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javassist.ByteArrayClassPath;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;
import javassist.bytecode.AnnotationsAttribute;
import javassist.bytecode.MethodInfo;
import javassist.bytecode.annotation.Annotation;
import javassist.bytecode.annotation.ArrayMemberValue;
import javassist.bytecode.annotation.MemberValue;
import javassist.bytecode.annotation.StringMemberValue;


/**
 * Created by tcummins on 28.06.16
 */
public class ImportantLogClassTransformer implements ClassFileTransformer
{
    private static final String METHOD_ANNOTATION = "co.uk.tcummins.java.auditLog.ImportantLog";
    private static final String ANNOTATION_ARRAY = "fields";
    private ClassPool pool;


    public ImportantLogClassTransformer()
    {
        this.pool = ClassPool.getDefault();
    }


    @Override
    public byte[] transform( final ClassLoader loader,
                             final String className,
                             final Class<?> classBeingRedefined,
                             final ProtectionDomain protectionDomain,
                             final byte[] classfileBuffer )
            throws IllegalClassFormatException
    {
        try
        {
            System.out.println( "Loading class: " + className );

            pool.insertClassPath( new ByteArrayClassPath( className, classfileBuffer ) );
            CtClass ctClass = pool.get( className.replaceAll( "/", "." ) );
            if( !ctClass.isFrozen() )
            {
                for( CtMethod currentMethod : ctClass.getDeclaredMethods() )
                {
                    Annotation annotation = getAnnotation( currentMethod );
                    if( annotation != null )
                    {
                        List parameterIndexes = getParamIndexes( annotation );
                        currentMethod.insertBefore( createJavaString( currentMethod, className, parameterIndexes ) );
                    }
                }
                return ctClass.toBytecode();
            }
        }
        catch( Exception e )
        {
            e.printStackTrace();
        }

        return null;
    }


    private Annotation getAnnotation( CtMethod method )
    {
        // The attribute we are looking for is a runtime invisible attribute
        // use Retention(RetentionPolicy.RUNTIME) on the annotation to make it visible at runtime
        MethodInfo methodInfo = method.getMethodInfo();

        AnnotationsAttribute annotationsAttribute = (AnnotationsAttribute) methodInfo
                .getAttribute( AnnotationsAttribute.invisibleTag );

        if( annotationsAttribute != null )
        {
            // This is the type name meaning use dots instead of slashes
            return annotationsAttribute.getAnnotation( METHOD_ANNOTATION );
        }

        return null;
    }


    private List getParamIndexes( Annotation annotation )
    {
        ArrayMemberValue fields = (ArrayMemberValue) annotation.getMemberValue( ANNOTATION_ARRAY );
        if( fields != null )
        {
            MemberValue[] values = (MemberValue[]) fields.getValue();
            List parameterIndexes = new ArrayList<>();
            for( MemberValue val : values )
            {
                parameterIndexes.add( ((StringMemberValue) val).getValue() );
            }
            return parameterIndexes;
        }

        return Collections.emptyList();
    }


    private String createJavaString( CtMethod currentMethod, String className, List indexParameters )
    {
        StringBuilder sb = new StringBuilder();
        sb.append( "{StringBuilder sb = new StringBuilder}" );
        sb.append( "(\"A call was made to method '\");" );
        sb.append( "sb.append(\"" );
        sb.append( currentMethod.getName() );
        sb.append( "\");sb.append(\"' on class '\");" );
        sb.append( "sb.append(\"" );
        sb.append( className );
        sb.append( "\");sb.append(\"'.\");" );
        sb.append( "sb.append(\"\\n    Important params:\");" );
        for( Object index : indexParameters )
        {
            try
            {
                // Add one because 0 is "this" for instance variable if were a static method 0 would not be anything
                int localVar = Integer.parseInt( index.toString() ) + 1;
                sb.append( "sb.append(\"\\n        Index \");" );
                sb.append( "sb.append(\"" );
                sb.append( index );
                sb.append( "\");sb.append(\" value: \");" );
                sb.append( "sb.append($" + localVar + ");" );
            }
            catch( NumberFormatException e )
            {
                e.printStackTrace();
            }
        }
        sb.append( "System.out.println(sb.toString());}" );
        return sb.toString();
    }
}
