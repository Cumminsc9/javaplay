package co.uk.tcummins.java.sleeping;

import java.lang.instrument.Instrumentation;


/**
 * Created by tcummins on 20.06.16
 */
public class Agent
{
    public static void main( String agentArgs, Instrumentation inst )
    {
        // Registers the transformer
        inst.addTransformer( new SleepingClassTransformer() );
    }
}
