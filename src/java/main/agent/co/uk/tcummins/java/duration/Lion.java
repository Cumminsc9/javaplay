package co.uk.tcummins.java.duration;

/**
 * Created by tcummins on 20.06.16
 */
public class Lion
{
    public void runLion() throws Exception
    {
        System.out.println( "Lion is going to run." );
        Thread.sleep( 2000L );
    }
}
