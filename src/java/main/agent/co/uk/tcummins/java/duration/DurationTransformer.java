package co.uk.tcummins.java.duration;

import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;

import java.io.ByteArrayInputStream;
import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.security.ProtectionDomain;


/**
 * Created by tcummins on 20.06.16
 */
public class DurationTransformer implements ClassFileTransformer
{
    @Override
    public byte[] transform( final ClassLoader loader, final String className, final Class<?> classBeingRedefined,
                             final ProtectionDomain protectionDomain, final byte[] classfileBuffer ) throws
                                                                                                     IllegalClassFormatException
    {
        byte[] byteCode = classfileBuffer;

        if( className.equals( "co/uk/tcummins/java/duration/Lion" ) )
        {
            System.out.println( "Instrumenting..." );
            CtClass ctClass = null;
            try
            {
                ClassPool classPool = ClassPool.getDefault();
                ctClass = classPool.makeClass( new ByteArrayInputStream( classfileBuffer ) );
                CtMethod[] methods = ctClass.getDeclaredMethods();

                for ( CtMethod method : methods )
                {
                    method.addLocalVariable( "startTime", CtClass.longType );
                    method.insertBefore("startTime = System.nanoTime();");
                    method.insertAfter("System.out.println(\"Execution Duration " + "(nano sec): \"+ (System.nanoTime() - startTime) );");
                }

                byteCode = ctClass.toBytecode();
            }
            catch( Exception ex )
            {
                System.out.println("Instrumentation failure.");
                ex.printStackTrace();
            }
            finally
            {
                if ( ctClass != null )
                {
                    ctClass.detach();
                }
            }
        }

        return byteCode;
    }
}
