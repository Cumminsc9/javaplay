package co.uk.tcummins.java.animal.v0;

/**
 * Created by tcummins on 20.11.15.
 */
public interface IAnimal
{
    void eat();

    void move();
}
