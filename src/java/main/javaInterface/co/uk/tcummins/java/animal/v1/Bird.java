package co.uk.tcummins.java.animal.v1;

/**
 * Created by tcummins on 21.03.16.
 * Project : JavaPlay
 */
public class Bird extends Animal
{
    /**
     * Bird specific code here
     *
     * Birds cannot chew so this would a problem in the sense
     * Bird classes can also call chew() method which is unwanted
     *
     * and incase of Birds simply extend Animal
     */
}
