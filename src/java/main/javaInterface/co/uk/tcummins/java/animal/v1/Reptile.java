package co.uk.tcummins.java.animal.v1;

/**
 * Created by tcummins on 21.03.16.
 * Project : JavaPlay
 */
public class Reptile extends Animal implements IChewable
{
    /**
     * Reptile specific code here
     *
     * chew() from class Animal not a problem here. But... ( see class Bird )
     *
     * and have Reptile class implement IChewable and not Birds (since Birds cannot chew)
     */

    @Override
    public void chew()
    {

    }
}