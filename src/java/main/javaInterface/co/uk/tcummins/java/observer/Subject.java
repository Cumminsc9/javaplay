package co.uk.tcummins.java.observer;

/**
 * Created by tcummins on 03.03.16.
 * Project : JavaPlay
 */
public interface Subject
{
    void registerObserver( Observer o );

    void notifyObserver( String tweet );
}
