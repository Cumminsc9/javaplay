package co.uk.tcummins.java.animal.v1;

/**
 * Created by tcummins on 21.03.16.
 * Project : JavaPlay
 */
public interface IChewable
{
    void chew();
}