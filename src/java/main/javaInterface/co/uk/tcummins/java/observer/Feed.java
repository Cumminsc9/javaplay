package co.uk.tcummins.java.observer;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by tcummins on 03.03.16.
 * Project : JavaPlay
 */
public class Feed implements Subject
{
    private final List<Observer> observers = new ArrayList<>( );

    @Override
    public void registerObserver( final Observer o )
    {
        this.observers.add( o );
    }


    @Override
    public void notifyObserver( final String tweet )
    {
        observers.forEach( o -> o.notify( tweet ) );
    }
}