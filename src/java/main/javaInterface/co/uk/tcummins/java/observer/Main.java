package co.uk.tcummins.java.observer;

/**
 * Created by tcummins on 03.03.16.
 * Project : JavaPlay
 */
public class Main
{
    public static void main( String[] args )
    {
        Feed f = new Feed();
        f.registerObserver( new NYTimes() );
        f.registerObserver( new Guardian() );
        f.registerObserver( new LeMonde() );

        //registerOb( f );

        /**
         * Dependent upon what is entered here 'queen', 'money' or 'wine' the correct NewsPaper will
         * pick it up and print it
         */
        f.notifyObserver( "The queen said her favourite book is Java 8 in Action!" );

        f.notifyObserver( "There's money in those trees!" );
    }

    private static void registerOb( Feed f )
    {
        f.registerObserver( ( String tweet ) ->
                            {
                                if( tweet != null && tweet.contains( "money" ) )
                                {
                                    System.out.println( "Breaking news in NY! " + tweet );
                                }
                            } );

        f.registerObserver( ( String tweet ) ->
                            {
                                if( tweet != null && tweet.contains( "queen" ) )
                                {
                                    System.out.println( "Yet more news in London... " + tweet );
                                }
                            } );

        f.registerObserver( ( String tweet ) ->
                            {
                                if( tweet != null && tweet.contains( "wine" ) )
                                {
                                    System.out.println( "Today cheese, wine and news! " + tweet );
                                }
                            } );
    }
}


class NYTimes implements Observer
{
    public void notify( String tweet )
    {
        if( tweet != null && tweet.contains( "money" ) )
        {
            System.out.println( "Breaking news in NY! " + tweet );
        }
    }
}


class Guardian implements Observer
{
    public void notify( String tweet )
    {
        if( tweet != null && tweet.contains( "queen" ) )
        {
            System.out.println( "Yet more news in London... " + tweet );
        }
    }
}


class LeMonde implements Observer
{
    public void notify( String tweet )
    {
        if( tweet != null && tweet.contains( "wine" ) )
        {
            System.out.println( "Today cheese, wine and news! " + tweet );
        }
    }
}