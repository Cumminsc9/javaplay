package co.uk.tcummins.java.animal.v0;

public class Mammal implements IAnimal
{
    public void eat()
    {
        System.out.println("Mammal eats");
    }


    public void move()
    {
        System.out.println("Mammal moves");
    }


    public int numOfLegs()
    {
        return 0;
    }


    public static void main( String[] args )
    {
        Mammal m = new Mammal();

        m.eat();
        m.move();
        m.numOfLegs();
    }
}
