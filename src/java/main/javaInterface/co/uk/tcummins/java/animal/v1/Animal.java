package co.uk.tcummins.java.animal.v1;

/**
 * Created by tcummins on 21.03.16. Project : JavaPlay
 */
public class Animal
{
    void walk()
    {
    }

//    void chew()
//    {
//    }

    /**
     * other Animal methods here
     */

    /**
     * Animal does not have the chew() method and instead is put in an interface as : IChewable
     */
}

