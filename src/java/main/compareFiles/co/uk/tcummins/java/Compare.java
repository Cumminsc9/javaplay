package co.uk.tcummins.java;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;


public class Compare
{
    static int count1 = 0;
    static int count2 = 0;

    static String arrayLines1[] = new String[countLines( "src/resources/eclipse.xml" )];
    static String arrayLines2[] = new String[countLines( "src/resources/intergral.xml" )];


    public static void main( String args[] )
    {
        findDifference( "src/resources/eclipse.xml", "src/resources/intergral.xml" );
        displayRecords();
    }


    public static int countLines( String File )
    {

        int lineCount = 0;
        try
        {
            BufferedReader br = new BufferedReader( new FileReader( File ) );
            while( (br.readLine()) != null )
            {
                lineCount++;
            }

        }
        catch( IOException e )
        {
            e.printStackTrace();
        }
        return lineCount;
    }


    public static void findDifference( String File1, String File2 )
    {
        String contents1 = null;
        String contents2 = null;
        try
        {
            FileReader file1 = new FileReader( File1 );
            FileReader file2 = new FileReader( File2 );
            BufferedReader buf1 = new BufferedReader( file1 );
            BufferedReader buf2 = new BufferedReader( file2 );

            while( (contents1 = buf1.readLine()) != null )
            {
                arrayLines1[count1] = contents1;
                count1++;
            }

            while( (contents2 = buf2.readLine()) != null )
            {
                arrayLines2[count2] = contents2;
                count2++;
            }
        }
        catch( Exception e )
        {
            e.printStackTrace();
        }
    }



    public static void displayRecords()
    {
        for( int i = 0; i <= arrayLines1.length; i++ )
        {
            String a = arrayLines1[i];
            for( int j = 0; j < arrayLines2.length; j++ )
            {
                String b = arrayLines2[j];
                boolean result = a.contains( b );
                if( result )
                {
                    System.out.println( a );
                }
            }

        }
    }
}