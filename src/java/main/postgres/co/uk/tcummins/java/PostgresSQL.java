package co.uk.tcummins.java;

import co.uk.tcummins.helpers.RandomInteger;

import java.sql.*;
import java.util.Scanner;


public class PostgresSQL
{
    private static final Scanner scanner = new Scanner( System.in );

    public static void main( String[] args ) throws SQLException
    {
        try
        {
            String sql;
            Connection conn;
            Statement state;

            final String[] fNames = { "Tom", "Mikey", "John", "Bernd", "Scott", "Ben",
                                      "Ben", "Terry", "Josh", "Neil", "Darren", "David" };

            final String[] sNames = { "Cummins", "Flewitt", "Hawksley", "Donath", "Spittle", "Everett",
                                      "Donnelly", "Pywell", "Wightman", "Tattersall", "Wilkinson", "Turner" };

            Class.forName( "org.postgresql.Driver" );
            conn = DriverManager.getConnection( "jdbc:postgresql://localhost:5432/testdb", "postgres", "password" );
            conn.setAutoCommit( false );

            DatabaseMetaData dbm = conn.getMetaData();
            ResultSet tables = dbm.getTables( null, null, "company", null );

            if ( tables.next() )
            {
                System.out.println("Table 'COMPANY' already exists. Would you like to drop the Table and recreate it?");
                String input = scanner.next();

                if( Boolean.valueOf( input ) )
                {
                    state = conn.createStatement();
                    sql = "DROP TABLE company";
                    state.executeQuery( sql );
                    System.out.println("Table 'COMPANY' dropped. Moving to create new table 'COMPANY'");
                }
            }
            else
            {
                System.out.println("Table 'COMPANY' does not exist. Creating table 'COMPANY'");

                state = conn.createStatement();
                sql = "CREATE TABLE company " +
                      "(ID           INT     PRIMARY KEY     NOT NULL," +
                      "FIRST_NAME    TEXT                    NOT NULL," +
                      "SECOND_NAME   TEXT                    NOT NULL," +
                      "AGE           INT                     NOT NULL)";
                state.executeUpdate( sql );
            }

            state = conn.createStatement();
            sql = "DELETE FROM company";
            state.executeUpdate( sql );
            System.out.println("Table 'COMPANY' data deleted. Moving to create new data.");

            for ( int i = 0; i < 5 ; i++ )
            {
                int     rand1 = RandomInteger.randomInt( 0, 1000 );
                String  rand2 = fNames[((int) Math.floor( Math.random() * fNames.length ))];
                String  rand3 = sNames[((int) Math.floor( Math.random() * sNames.length ))];
                int     rand4 = 20 + (int)(Math.random() * ((50 - 19) + 1));

                sql = "INSERT INTO company (ID, FIRST_NAME, SECOND_NAME, AGE ) VALUES ('"+rand1+"','"+rand2+"','"+rand3+"','"+rand4+"');";
                state.executeUpdate(sql);
            }

//            state = conn.createStatement();
//            sql = "DELETE FROM company where ID='"+rand1+"'";
//            state.executeQuery( sql );

            state = conn.createStatement();
            ResultSet rs = state.executeQuery( "SELECT * FROM company" );

            while ( rs.next() )
            {
                int id              = rs.getInt( "ID" );
                String firstName    = rs.getString( "FIRST_NAME" );
                String secondName   = rs.getString( "SECOND_NAME" );
                int age             = rs.getInt( "AGE" );

                System.out.println("\nID: "           + id);
                System.out.println("First Name: "   + firstName);
                System.out.println("Second Name: "  + secondName);
                System.out.println("Age: "          + age);
                System.out.println("\n----------------------------");
            }

            state.close();
            conn.commit();
            conn.close();
        }
        catch ( Exception e )
        {
            e.printStackTrace();
            System.err.println(e.getClass().getName()+": "+e.getMessage());
            System.exit( 0 );
        }
    }
}
