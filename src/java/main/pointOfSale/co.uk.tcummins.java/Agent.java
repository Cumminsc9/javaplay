package co.uk.tcummins.java;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;


/**
 * Created by tcummins on 25.05.16.
 */
public class Agent
{
    private MongoCollection<Document> collection;

    public static void main( String[] args ) throws Exception
    {
        new Agent( "localhost", 27017 );
    }

    private Agent( final String host, final int port ) throws Exception
    {
        final MongoClient mongoClient = new MongoClient( host, port );
        final MongoDatabase products = mongoClient.getDatabase( "Products" );
        collection = products.getCollection( "Items" );

        getItems();
    }

    private void getItems()
    {

    }
}
