package co.uk.tcummins.java.v0;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class QuoteClient
{
    public static void main( String[] args ) throws IOException
    {
        if( args.length != 1 )
        {
            System.out.println( "Usage: java QuoteClient <hostname>" );
            return;
        }

        //get a datagram socket
        DatagramSocket socket = new DatagramSocket(  );

        //send request
        byte[] buff = new byte[256];
        InetAddress address = InetAddress.getByName( args[0] );
        DatagramPacket packet = new DatagramPacket( buff, buff.length, address, 1234 );
        socket.send( packet );

        //get response
        packet = new DatagramPacket( buff, buff.length );
        socket.receive( packet );

        //display response
        String received = new String( packet.getData(), 0, packet.getLength() );
        System.out.println( "Quote of the moment: " + received);

        socket.close();
    }
}
