package co.uk.tcummins.java.v1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;


public class ChatClient
{
    public static void main( String[] args ) throws IOException
    {
        DatagramSocket s = new DatagramSocket(  );
        byte[] buf = new byte[1000];
        DatagramPacket dp = new DatagramPacket( buf, buf.length );

        InetAddress address = InetAddress.getByName( "localhost" );

        while ( true )
        {
            BufferedReader userIn = new BufferedReader( new InputStreamReader( System.in ) );
            String outMessage = userIn.readLine();

            if( outMessage.equals( "bye" ) )
                break;

            String outString = "Client :: " + outMessage;
            buf = outString.getBytes();

            DatagramPacket out = new DatagramPacket( buf, buf.length, address, 4000 );

            s.send( out );

            s.receive( dp );

            String recv = "Received from " + dp.getAddress() + ", " + dp.getPort() + ": " + new String( dp.getData() , 0, dp.getLength());
            System.out.println( recv );
        }
    }
}


