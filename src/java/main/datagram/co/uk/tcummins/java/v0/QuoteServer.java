package co.uk.tcummins.java.v0;

import java.io.IOException;


public class QuoteServer
{
    public static void main( String[] args ) throws IOException
    {
        new QuoteServerThread().run();
    }
}
