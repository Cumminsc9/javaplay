package co.uk.tcummins.java.v0;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Date;


public class QuoteServerThread extends Thread
{
    protected DatagramSocket socket = null;
    protected BufferedReader in = null;
    protected boolean moreQuotes = true;

    public QuoteServerThread() throws IOException
    {
        this("QuoteServer");
    }

    public QuoteServerThread( String name ) throws IOException
    {
        super( name );
        socket = new DatagramSocket( 1234 );

        try
        {
            in = new BufferedReader( new FileReader( "src/resources/one-liners.txt" ) );
        }
        catch ( FileNotFoundException e )
        {
            System.err.println( "Could not open file, Serving time instead" );
        }
    }

    public void run()
    {
        while ( moreQuotes )
        {
            try
            {
                byte[] buff = new byte[256];

                //receive request
                DatagramPacket packet = new DatagramPacket( buff, buff.length );
                socket.receive( packet );

                //figure out response
                String dString;

                if( in == null ) {
                    dString = new Date( ).toString();
                }
                else {
                    dString = getNextQuote();
                }

                buff = dString.getBytes();

                //send the response to the client at "address" and "port"
                InetAddress address = packet.getAddress();
                int port = packet.getPort();
                packet = new DatagramPacket( buff, buff.length, address, port );
                socket.send( packet );
            }
            catch ( IOException e )
            {
                e.printStackTrace();
            }
        }
    }

    private String getNextQuote()
    {
        String returnValue;

        try
        {
            if( ( returnValue = in.readLine() ) == null )
            {
                in.close();
                moreQuotes = false;
                returnValue = "No more quotes... Goodbye";
            }
        }
        catch ( IOException e )
        {
            returnValue = "IOExceptio occured in server";
        }

        return returnValue;
    }
}
