package co.uk.tcummins.java.vThree;

/**
 * https://docs.oracle.com/javase/tutorial/essential/concurrency/guardmeth.html
 */
public class MainVThree
{
    public static void main( String[] args )
    {
        Drop drop = new Drop();
        (new Thread( new Producer( drop ) )).start();
        (new Thread( new Consumer( drop ) )).start();
    }
}
