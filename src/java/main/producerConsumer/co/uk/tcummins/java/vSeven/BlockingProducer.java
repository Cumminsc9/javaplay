package co.uk.tcummins.java.vSeven;

import java.util.concurrent.BlockingQueue;


public class BlockingProducer implements Runnable
{
    private BlockingQueue<Message> queue;
    private int count = 0;


    public BlockingProducer( BlockingQueue<Message> queue, final int count )
    {
        this.queue = queue;
        this.count = count;
    }


    @Override
    public void run()
    {
        for( int i = 1; i <= count; i++ )
        {
            Message msg = new Message( "Message " + i );
            try
            {
                Thread.sleep( 10 );
                queue.put( msg );
                System.out.println( "BlockingProducer: Message - " + msg.getMsg() + " produced." );
            }
            catch( Exception e )
            {
                System.out.println( "Exception:" + e );
            }
        }

        // adding exit message
        Message msg = new Message( "Producer side completed. Produced " +count+ " Messages" );
        try
        {
            queue.put( msg );
            System.out.println( "BlockingProducer: Exit Message - " + msg.getMsg() );
        }
        catch( Exception e )
        {
            System.out.println( "Exception:" + e );
        }
    }
}
