package co.uk.tcummins.java.vSix;

import java.util.concurrent.BlockingQueue;

public class Consumer implements Runnable
{
    protected BlockingQueue queue = null;
    private int count = 0;


    public Consumer( BlockingQueue queue, final int count )
    {
        this.queue = queue;
        this.count = count;
    }


    public void run()
    {
        try
        {
            for ( int i = 0; i < count; i++ )
            {
                System.out.println( "Consumed: " + queue.take() );
            }
        }
        catch( InterruptedException e )
        {
            e.printStackTrace();
        }
    }
}