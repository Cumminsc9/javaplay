package co.uk.tcummins.java.vFour;

class Producer implements Runnable
{
    private String greeting;
    private SyncedQueue<String> queue;

    private int greetingCount;


    public Producer( String aGreeting, SyncedQueue<String> aQueue, int count )
    {
        greeting = aGreeting;
        queue = aQueue;
        greetingCount = count;
    }


    public void run()
    {
        try
        {
            int i = 1;
            while( i <= greetingCount )
            {
                queue.add( i + ": " + greeting );
                i++;
                Thread.sleep( 2000 );
            }
        }
        catch( InterruptedException exception )
        {
        }
    }

}