package co.uk.tcummins.java.vSeven;

import java.util.Objects;
import java.util.concurrent.BlockingQueue;


public class BlockingConsumer implements Runnable
{

    private BlockingQueue<Message> queue;


    public BlockingConsumer( BlockingQueue<Message> queue )
    {
        this.queue = queue;
    }


    @Override
    public void run()
    {
        try
        {
            Message msg;

            // consuming messages until exit message is received
            while( !Objects.equals( ( msg = queue.take() ).getMsg(), "exit" ) )
            {
                Thread.sleep( 10 );
                System.out.println( "BlockingConsumer: Message - " + msg.getMsg() + " consumed." );
            }
        }
        catch( InterruptedException e )
        {
            e.printStackTrace();
        }
    }
}