package co.uk.tcummins.java.vSix;

import java.util.Random;
import java.util.concurrent.BlockingQueue;

public class Producer implements Runnable
{
    private BlockingQueue bq = null;
    private int count = 0;


    public Producer( BlockingQueue queue, final int count )
    {
        this.setBlockingQueue( queue );
        this.count = count;
    }


    public void run()
    {

        Random rand = new Random();
        int res = 0;
        try
        {
            for(int i = 0; i < count; i++)
            {
                res = Addition( rand.nextInt( 100 ), rand.nextInt( 50 ) );
                System.out.println( "Produced: " + res );
                bq.put( res );
                Thread.sleep( 500 );
            }
        }
        catch( InterruptedException e )
        {
            e.printStackTrace();
        }
    }


    public void setBlockingQueue( BlockingQueue bq )
    {
        this.bq = bq;
    }


    public int Addition( int x, int y )
    {
        int result = 0;
        result = x + y;
        return result;
    }
}