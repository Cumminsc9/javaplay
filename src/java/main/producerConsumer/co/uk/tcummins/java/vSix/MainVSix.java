package co.uk.tcummins.java.vSix;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;


public class MainVSix
{
    private static final int count = 100;

    public static void main( String[] args ) throws Exception
    {
        BlockingQueue bq = new ArrayBlockingQueue( 1000 );

        Producer producer = new Producer( bq, count );
        Consumer consumer = new Consumer( bq, count );

        new Thread( producer ).start();
        new Thread( consumer ).start();

        Thread.sleep( 4000 );
    }
}
