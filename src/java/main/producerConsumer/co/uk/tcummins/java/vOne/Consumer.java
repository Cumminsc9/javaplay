package co.uk.tcummins.java.vOne;

public class Consumer extends Thread
{
    private CubbyHole cubbyhole;
    private int number;

    public Consumer(CubbyHole c, int num)
    {
        cubbyhole = c;
        number = num;
    }


    public void run()
    {
        int value = 0;

        for (int i = 0; i < 10; i++)
        {
            value = cubbyhole.get();
            System.out.println("Consumer #" + this.number + " got: " + value);
        }
    }
}
