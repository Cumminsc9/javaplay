package co.uk.tcummins.java.vFive;

class Producer extends Thread
{
    Queue queue;


    Producer( Queue queue )
    {
        this.queue = queue;
    }


    public void run()
    {
        int i = 0;
        while( true )
        {
            queue.add( i++);
            System.out.println( "Producer: " + i++ );
        }
    }
}