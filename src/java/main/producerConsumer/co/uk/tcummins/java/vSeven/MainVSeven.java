package co.uk.tcummins.java.vSeven;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;


public class MainVSeven
{
    private static final int count = 500;

    public static void main( String[] args )
    {
        // Creating BlockingQueue of size 10
        // BlockingQueue supports operations that wait for the queue to become non-empty when retrieving an element, and
        // wait for space to become available in the queue when storing an element.
        BlockingQueue<Message> queue = new ArrayBlockingQueue<>( 10 );
        BlockingProducer producer = new BlockingProducer( queue, count );
        BlockingConsumer consumer = new BlockingConsumer( queue );

        // starting producer to produce messages in queue
        new Thread( producer ).start();

        // starting consumer to consume messages from queue
        new Thread( consumer ).start();

        System.out.println( "Starting... Producer / Consumer Test Started.\n" );
    }
}
