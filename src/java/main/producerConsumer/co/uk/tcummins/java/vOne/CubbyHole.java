package co.uk.tcummins.java.vOne;

public class CubbyHole
{
    private int contents;
    private boolean available = false;


    public synchronized int get()
    {
        while ( !available )
        {
            try
            {
                wait();
            }
            catch (InterruptedException ignored )
            {
            }
        }

        available = false;
        notifyAll();
        return contents;
    }


    public synchronized void put(int value)
    {
        while ( available )
        {
            try
            {
                wait();
            }
            catch ( InterruptedException ignored )
            {
            }
        }

        contents = value;
        available = true;
        notifyAll();
    }
}
