package co.uk.tcummins.java.vFour;

class Consumer implements Runnable {

    private SyncedQueue<String> queue;

    private int greetingCount;

    public Consumer(SyncedQueue<String> aQueue, int count) {
        queue = aQueue;
        greetingCount = count;
    }

    public void run() {
        try {
            int i = 1;
            while (i <= greetingCount) {
                String greeting = queue.remove();
                System.out.println(greeting);
                i++;
                Thread.sleep(3000);
            }
        } catch (InterruptedException exception) {
        }
    }

}