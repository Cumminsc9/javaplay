package co.uk.tcummins.java.vSeven;

class Message
{
    private String stringMsg;

    public Message( final String germanMsg )
    {
        this.stringMsg = germanMsg;
    }

    public String getMsg()
    {
        return stringMsg;
    }
}
