package co.uk.tcummins.java.vFive;

import java.util.Vector;


class Queue
{
    private final static int SIZE = 5;
    private Vector queue = new Vector();
    private int count = 0;


    synchronized void add( int i )
    {
        while( count == SIZE )
        {
            try
            {
                wait();
            }
            catch( InterruptedException ie )
            {
                ie.printStackTrace();
                System.exit( 0 );
            }
        }
        queue.addElement( new Integer( i ) );
        ++count;
        notifyAll();
    }


    synchronized int remove()
    {
        while( count == 0 )
        {
            try
            {
                wait();
            }
            catch( InterruptedException ie )
            {
                ie.printStackTrace();
                System.exit( 0 );
            }
        }
        Integer iobj = (Integer) queue.firstElement();
        queue.removeElement( iobj );
        --count;
        notifyAll();
        return iobj.intValue();
    }
}