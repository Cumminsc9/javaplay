package co.uk.tcummins.java.classes;

/**
 * Created by tcummins on 14.06.16
 */
public class ClassCreationDemo
{
    private int version;


    public int getVersion()
    {
        return version;
    }


    public void setVersion( final int version )
    {
        this.version = version;
    }


    @Override
    public String toString()
    {
        return "ClassCreationDemo{" + "version=" + version + '}';
    }
}