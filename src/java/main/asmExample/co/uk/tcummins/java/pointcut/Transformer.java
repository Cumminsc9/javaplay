package co.uk.tcummins.java.pointcut;

import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.security.ProtectionDomain;


/**
 * Created by tcummins on 20.06.16
 */
public class Transformer implements ClassFileTransformer
{

    @Override
    public byte[] transform( final ClassLoader loader, final String className, final Class<?> classBeingRedefined,
                             final ProtectionDomain protectionDomain, final byte[] classfileBuffer ) throws
                                                                                                     IllegalClassFormatException
    {
        return new byte[0];
    }
}
