package co.uk.tcummins.java.classes;

/**
 * Created by tcummins on 14.06.16
 */
public class ClassModificationDemo
{
    private int version;


    public int getVersion()
    {
        return version;
    }


    public void setVersion( final int version )
    {
        this.version = version;
    }


    @Override
    public String toString()
    {
        return "ClassModificationDemo{" + "version=" + version + '}';
    }


    public static void main( String[] args )
    {
        System.out.println( new ClassModificationDemo() );
    }
}