package co.uk.tcummins.java.classes;

import co.uk.tcummins.java.ASMHelloWorld;
import jdk.internal.org.objectweb.asm.ClassReader;
import jdk.internal.org.objectweb.asm.ClassVisitor;
import jdk.internal.org.objectweb.asm.ClassWriter;
import jdk.internal.org.objectweb.asm.MethodVisitor;
import jdk.internal.org.objectweb.asm.Opcodes;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;


/**
 * Created by tcummins on 14.06.16
 */
public class ClassModifierDemo
{
    public static class ModifiedMethodWriter extends MethodVisitor
    {
        private String methodName;

        public ModifiedMethodWriter( int api, MethodVisitor methodVisitor, String methodName )
        {
            super( api, methodVisitor );
            this.methodName = methodName;
        }



        /**
         *  This is the point we insert the code. Note: The instructions are added right after the visitCode()
         *  method of the super class. This ordering is very important.
         */
        @Override
        public void visitCode()
        {
            super.visitCode();
            super.visitFieldInsn( Opcodes.GETSTATIC, "java/lang/System", "out", "Ljava/io/PrintStream;" );
            super.visitLdcInsn( "method: " + methodName );
            super.visitMethodInsn( Opcodes.INVOKEVIRTUAL, "java/io/PrintStream", "println", "(Ljava/lang/String;)V", false );
        }
    }


    /**
     * Our class modifier class visitor. It delegates all calls to the super class.
     * Only makes sure that it returns our MethodVisitor for every method.
     */
    public static class ModifierClassWriter extends ClassVisitor
    {
        private int api;

        public ModifierClassWriter( int api, ClassWriter classWriter )
        {
            super( api, classWriter );
            this.api = api;
        }


        @Override
        public MethodVisitor visitMethod( final int access, final String name, final String desc, final String signature,
                                          final String[] exceptions )
        {
            MethodVisitor methodVisitor = super.visitMethod( access, name, desc, signature, exceptions );
            return new ModifiedMethodWriter( api, methodVisitor, name );
        }
    }


    public static void main( String[] args ) throws Exception
    {
        InputStream inputStream = ASMHelloWorld.class.getResourceAsStream( "/co/uk/tcummins/java/classes/ClassModificationDemo.class" );
        ClassReader classReader = new ClassReader( inputStream );
        ClassWriter classWriter = new ClassWriter( ClassWriter.COMPUTE_MAXS );

        // Wrap the classWriter with our custom ClassVisitor
        ModifierClassWriter modifierClassWriter = new ModifierClassWriter( Opcodes.ASM4, classWriter );
        classReader.accept( modifierClassWriter, 0 );

        // Write the output to a class file
        File outputDirectory = new File( "resources" );
        outputDirectory.mkdir();
        DataOutputStream dataOutputStream = new DataOutputStream( new FileOutputStream( new File( "ClassModificationDemo.class" ) ) );
        dataOutputStream.write( classWriter.toByteArray() );
    }
}
