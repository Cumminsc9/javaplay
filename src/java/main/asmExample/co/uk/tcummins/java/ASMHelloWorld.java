package co.uk.tcummins.java;

import org.objectweb.asm.AnnotationVisitor;
import org.objectweb.asm.Attribute;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.FieldVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

import java.io.IOException;
import java.io.InputStream;


/**
 * Created by tcummins on 13.06.16
 */
public class ASMHelloWorld
{
    public static void main( String[] args ) throws IOException
    {
        ClassVisitor cl = new ClassVisitor( Opcodes.ASM4 )
        {
            /**
             * Called when a class in visited. This is the method called first.
             *
             * @param version
             * @param access
             * @param name
             * @param signature
             * @param superName
             * @param interfaces
             */
            @Override
            public void visit( final int version, final int access, final String name, final String signature,
                               final String superName, final String[] interfaces )
            {
                System.out.println( "Visiting Class: " + name );
                System.out.println( "Class Major Version: " + version );
                System.out.println( "Super Class: " + superName );
                super.visit( version, access, name, signature, superName, interfaces );
            }


            /**
             * Invoked only when the class being visited is an inner class
             *
             * @param owner
             * @param name
             * @param desc
             */
            @Override
            public void visitOuterClass( final String owner, final String name, final String desc )
            {
                System.out.println( "Outer Class: " + owner );
                super.visitOuterClass( owner, name, desc );
            }


            /**
             * Invoked when a class level annotation is encountered.
             *
             * @param desc
             * @param visible
             * @return
             */
            @Override
            public AnnotationVisitor visitAnnotation( final String desc, final boolean visible )
            {
                System.out.println( "Annotation: " + desc );
                return super.visitAnnotation( desc, visible );
            }


            /**
             * When a class attribute is encountered.
             *
             * @param attribute
             */
            @Override
            public void visitAttribute( final Attribute attribute )
            {
                System.out.println( "Class Attribute: " + attribute.type );
                super.visitAttribute( attribute );
            }


            /**
             * When a inncer class is encountered.
             *
             * @param name
             * @param outerName
             * @param innerName
             * @param access
             */
            @Override
            public void visitInnerClass( final String name, final String outerName, final String innerName, final int access )
            {
                System.out.println( "Inner Class: "  + innerName + " Defined In " + outerName );
                super.visitInnerClass( name, outerName, innerName, access );
            }


            /**
             * When a field is encountered.
             *
             * @param access
             * @param name
             * @param desc
             * @param signature
             * @param value
             * @return
             */
            @Override
            public FieldVisitor visitField( final int access, final String name, final String desc, final String signature,
                                            final Object value )
            {
                System.out.println( "Field: " + name + " " + desc + " Value " + value );
                return super.visitField( access, name, desc, signature, value );
            }


            /**
             *
             */
            @Override
            public void visitEnd()
            {
                System.out.println( "Method Ends Here" );
                super.visitEnd();
            }


            /**
             * When a method is encountered.
             *
             * @param access
             * @param name
             * @param desc
             * @param signature
             * @param exceptions
             * @return
             */
            @Override
            public MethodVisitor visitMethod( final int access, final String name, final String desc, final String signature,
                                              final String[] exceptions )
            {
                System.out.println( "Method: " + name + " " + desc );
                return super.visitMethod( access, name, desc, signature, exceptions );
            }


            /**
             * When the optional source in encountered/
             *
             * @param source
             * @param debug
             */
            @Override
            public void visitSource( final String source, final String debug )
            {
                System.out.println( "Source: " + source );
                super.visitSource( source, debug );
            }
        };

        InputStream inputStream = ASMHelloWorld.class.getResourceAsStream( "/java/lang/String.class" );
        ClassReader classReader = new ClassReader( inputStream );
        classReader.accept( cl, 0 );
    }
}
