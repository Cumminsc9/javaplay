package co.uk.tcummins.java;

import java.io.InputStream;

import org.objectweb.asm.commons.InstructionAdapter;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.MethodVisitor;


/**
 * Created by tcummins on 14.06.16
 */
public class DemoClassInstructionViewer
{
    public static class MethodPrinterVisitor extends ClassVisitor
    {
        public MethodPrinterVisitor( final int api, final ClassVisitor classVisitor )
        {
            super( api, classVisitor );
        }


        public MethodPrinterVisitor( final int api )
        {
            super( api );
        }


        @Override
        public MethodVisitor visitMethod( final int access, final String name, final String desc, final String signature,
                                          final String[] exceptions )
        {
            System.out.println( "\nMethod: " + name + " " + desc );

            MethodVisitor methodVisitor = new MethodVisitor( 5 )
            {
            };

            return new InstructionAdapter( methodVisitor )
            {
                @Override
                public void visitInsn( final int opcode )
                {
                    super.visitInsn( opcode );
                }
            };
        }


        public static void main( String[] args ) throws Exception
        {
            InputStream in = ASMHelloWorld.class.getResourceAsStream( "/co/uk/tcummins/java/ASMHelloWorld.class" );
            ClassReader classReader = new ClassReader( in );
            classReader.accept( new MethodPrinterVisitor( 5 ), 0 );
        }
    }
}