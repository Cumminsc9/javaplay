package co.uk.tcummins.java.classes;

import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.FieldVisitor;
import org.objectweb.asm.MethodVisitor;

import java.io.FileOutputStream;
import java.io.DataOutputStream;

import static org.objectweb.asm.Opcodes.*;


/**
 * Created by tcummins on 14.06.16
 */
public class ClassCreationDemoMaker
{
    public static byte[] dump() throws Exception
    {
        // ClassWriter is a class visitor that generates the code for the class.
        ClassWriter classWriter = new ClassWriter( 0 );
        FieldVisitor fieldVisitor;
        MethodVisitor methodVisitor;

        // Start creating the class.
        classWriter.visit( V1_6, ACC_PUBLIC + ACC_SUPER, "co/uk/tcummins/java/ClassCreationDemo", null, "java/lang/Object", null );
        {
            // Version field.
            fieldVisitor = classWriter.visitField( ACC_PRIVATE, "version", "I", null, null );
            fieldVisitor.visitEnd();
        }
        {
            // Implementing the constructor.
            methodVisitor = classWriter.visitMethod( ACC_PUBLIC, "<init>", "()V", null, null );
            methodVisitor.visitCode();
            methodVisitor.visitVarInsn( ALOAD, 0 );
            methodVisitor.visitMethodInsn( INVOKESPECIAL, "java/lang/Object", "<init>", "()V", false );
            methodVisitor.visitInsn( RETURN );
            methodVisitor.visitMaxs( 1, 1 );
            methodVisitor.visitEnd();
        }
        {
            // getVersion Method
            methodVisitor = classWriter.visitMethod( ACC_PUBLIC, "getVersion", "()I", null, null );
            methodVisitor.visitCode();
            methodVisitor.visitVarInsn( ALOAD, 0 );
            methodVisitor.visitFieldInsn( GETFIELD, "co/uk/tcummins/java/ClassCreationDemo", "version", "I" );
            methodVisitor.visitInsn( IRETURN );
            methodVisitor.visitMaxs( 1, 1 );
            methodVisitor.visitEnd();
        }
        {
            // setVersion Method
            methodVisitor = classWriter.visitMethod( ACC_PUBLIC, "setVersion", "(I)V", null, null );
            methodVisitor.visitCode();
            methodVisitor.visitVarInsn( ALOAD, 0 );
            methodVisitor.visitFieldInsn( GETFIELD, "co/uk/tcummins/java/ClassCreationDemo", "version", "I" );
            methodVisitor.visitInsn( IRETURN );
            methodVisitor.visitMaxs( 1, 1 );
            methodVisitor.visitEnd();
        }
        {
            // toString Method
            methodVisitor = classWriter.visitMethod( ACC_PUBLIC, "toString", "()Ljava/lang/String;", null, null );
            methodVisitor.visitCode();
            methodVisitor.visitTypeInsn( NEW, "java/lang/StringBuilder" );
            methodVisitor.visitLdcInsn( "ClassCreationDemo: " );
            methodVisitor.visitMethodInsn( INVOKESPECIAL, "java/lang/StringBuilder", "<init>", "(Ljava/lang/String;)V", false );
            methodVisitor.visitVarInsn( ALOAD, 0 );
            methodVisitor.visitFieldInsn( GETFIELD, "co/uk/tcummins/java/ClassCreationDemo", "version", "I" );
            methodVisitor.visitMethodInsn( INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(I)Ljava/lang/StringBuilder;", false );
            methodVisitor.visitMethodInsn( INVOKEVIRTUAL, "java/lang/StringBuilder", "toString", "()Ljava/lang/String;", false );
            methodVisitor.visitInsn( ARETURN );
            methodVisitor.visitMaxs( 3, 1 );
            methodVisitor.visitEnd();
        }
        classWriter.visitEnd();

        return classWriter.toByteArray();
    }


    public static void main( String[] args ) throws Exception
    {
        DataOutputStream dataOutputStream = new java.io.DataOutputStream( new FileOutputStream( "ClassCreationDemo.class" ) );

        dataOutputStream.write( dump() );
        dataOutputStream.flush();
        dataOutputStream.close();
    }
}