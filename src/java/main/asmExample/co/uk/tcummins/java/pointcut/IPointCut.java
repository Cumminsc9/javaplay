package co.uk.tcummins.java.pointcut;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.tree.ClassNode;


/**
 * Created by tcummins on 20.06.16
 */
public interface IPointCut
{
    public Object getAdapter( ClassNode classNode, MethodVisitor methodVisitor, String className, int access,
                              String name, String desc );

    public boolean classMatches( String className, ClassReader classReader, Class<?> classBeingRedefined, ClassLoader classLoader );


    public boolean classIncludes( String className );


    public boolean matches( ClassLoader classLoader,
                            ClassReader classReader,
                            Class<?> clazz,
                            String className,
                            int access,
                            String name,
                            String desc,
                            String signature,
                            String[] exceptions );


    public boolean isActive();


    public Object[] getProperties();
}