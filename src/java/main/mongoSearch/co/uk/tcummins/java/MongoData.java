package co.uk.tcummins.java;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mongodb.client.MongoCollection;
import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;

public class MongoData
{
    private static List<String> list = new ArrayList<>();
    private static HashMap<String, String> newData = new HashMap<>();
    private static HashMap<String, String> sorted = new HashMap<>();
    private static MongoCollection<Document> collection;

    public static void main( String[] args )
    {
        list.add( "3b7aa2be-640c-41a7-923f-68af904a2281" );
        list.add( "6b58004b-2ea6-46cb-aa88-c205d8f6d1e2" );
        list.add( "f2695ac8-545f-4dbb-b942-6dac77528803" );
        list.add( "1d1e2a9c-3bd2-4b8d-9a16-e996684b9056" );
        list.add( "1307d9ed-15c9-4b2d-b255-25e9bf87300e" );
        list.add( "b28ac5b8-de61-42d2-b0bb-93bfa4785814" );
        list.add( "718699a0-f029-4936-8017-9f994f86c7e1" );
        list.add( "69a10b55-afae-4395-bea8-6fd19d2b34fb" );
        list.add( "f903da6c-2fda-4254-9aea-206362c46a25" );
        list.add( "bc37f374-0a20-4709-9061-9941911a3182" );
        list.add( "ea88c0e6-906b-4e1b-8e4a-ccbdb7a54a3e" );

        connect();

        for (Map.Entry<String, String> entry : sorted.entrySet())
        {
            String key = entry.getKey();
            String value = entry.getValue();
            System.out.println( "FRUID " + key + "\tGRUID " + value );
        }
    }

    private static void connect()
    {
        MongoClient mongoClient = new MongoClient( "mongodb-1", 27017 );
        MongoDatabase db = mongoClient.getDatabase( "fr-license" );
        collection = db.getCollection( "activations" );

        searchCollections();
    }

    private static void searchCollections()
    {
        List<Document> all = collection.find().into( new ArrayList<>() );
        for( Document doc : all )
        {
            String fruid = String.valueOf( doc.get( "local" ) );
            String gruid = String.valueOf( doc.get( "global" ) );

            newData.put( fruid, gruid );
        }

        for (Map.Entry<String, String> entry : newData.entrySet())
        {
            String key = entry.getKey();
            String value = entry.getValue();

            if( list.contains( key ) )
            {
                sorted.put( key, value );
            }
        }
    }
}
