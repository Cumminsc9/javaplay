package co.uk.tcummins.security;

import java.io.IOException;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.NameCallback;
import javax.security.auth.callback.PasswordCallback;
import javax.security.auth.callback.UnsupportedCallbackException;


/**
 * Created by tcummins on 07.03.16.
 * Project : JavaPlay
 */
public class TestCallBackHandler implements CallbackHandler
{

    String name;
    String password;


    public TestCallBackHandler( String name, String password )
    {
        System.out.println( "Callback Handler - constructor called" );
        this.name = name;
        this.password = password;
    }


    public void handle( Callback[] callbacks ) throws IOException, UnsupportedCallbackException
    {
        System.out.println( "Callback Handler - handle called" );
        for( int i = 0; i < callbacks.length; i++ )
        {
            if( callbacks[i] instanceof NameCallback )
            {
                NameCallback nameCallback = (NameCallback) callbacks[i];
                nameCallback.setName( name );
            }
            else if( callbacks[i] instanceof PasswordCallback )
            {
                PasswordCallback passwordCallback = (PasswordCallback) callbacks[i];
                passwordCallback.setPassword( password.toCharArray() );
            }
            else
            {
                throw new UnsupportedCallbackException( callbacks[i], "The submitted Callback is unsupported" );
            }
        }
    }
}
