package co.uk.tcummins.security;

import javax.security.auth.login.LoginContext;
import javax.security.auth.login.LoginException;
import java.util.Scanner;


/**
 * Created by tcummins on 07.03.16.
 * Project : JavaPlay
 *
 * See @ http://www.avajava.com/tutorials/lessons/how-do-i-create-a-login-module.html?page=1
 */
public class Main
{
    private static String myName = "admin";
    private static String myPassword = "admin";
    private static Scanner scanner = new Scanner( System.in );

    public static void main( String[] args )
    {
        System.setProperty( "java.security.auth.login.config", "src/resources/auth.config" );

        System.out.print( "Enter Username: " );
            myName = scanner.nextLine();

        System.out.print( "Enter Password: " );
            myPassword = scanner.nextLine();

        try
        {
            LoginContext lc = new LoginContext( "SecurityCheck", new TestCallBackHandler( myName, myPassword ) );
            lc.login();
        }
        catch( LoginException e )
        {
            e.printStackTrace();
        }
    }
}