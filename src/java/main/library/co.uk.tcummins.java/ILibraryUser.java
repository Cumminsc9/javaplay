package co.uk.tcummins.java;

/**
 * Created by tcummins on 26.11.15.
 */
public interface ILibraryUser
{
    void firstName();

    void secondName();

    void teleNum();

    void age();

    public static enum ageRange
    {
        ADULT, YOUNG_ADULT, TEEN, CHILD
    }
}
