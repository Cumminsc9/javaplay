package co.uk.tcummins.java;

/**
 * Created by tcummins on 26.11.15.
 */
public interface IItems
{
    void ISBN();

    void title();

    void authors();

    void publisher();

    void location();
}
