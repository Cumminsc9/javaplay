package co.uk.tcummins.java;

public class SecondsCounter implements Runnable
{
    private int totalSecs, currentSecs;
    private long startTime, runningTime, endTime;
    private Thread thread;

    public SecondsCounter(int totalSecs)
    {
        this.totalSecs = totalSecs;
    }

    public int getCurrentSecond()
    {
        return this.currentSecs;
    }

    @Override
    public void run()
    {
        while ((this.runningTime = System.currentTimeMillis()) < this.endTime)
        {
            this.currentSecs = (int)(this.endTime - this.runningTime) / 1000;
        }

        this.stop();
    }

    public void start()
    {
        this.startTime = System.currentTimeMillis();
        this.runningTime = this.startTime;
        this.endTime = this.startTime + (this.totalSecs * 1000);

        // multithreading here:
        this.thread = new Thread(this);
        this.thread.start();
    }

    public boolean isRunning()
    {
        return this.thread.isAlive();
    }

    public void stop()
    {
        this.thread.interrupt();
        this.thread = null;
    }
}
