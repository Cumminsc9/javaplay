package co.uk.tcummins.java;

public class MultiThreadTest
{
    public static void main( String[] args )
    {
        int totalSeconds = 5;
        SecondsPrinter printer = new SecondsPrinter(totalSeconds);
        printer.startPrinting();
    }
}
