package co.uk.tcummins.java;

public class SecondsPrinter
{
    private SecondsCounter clock;

    public SecondsPrinter(int totalSeconds)
    {
        this.clock = new SecondsCounter(totalSeconds);
    }

    public void startPrinting()
    {
        this.clock.start();
        while (this.clock.isRunning())
        {
            System.out.println( this.clock.getCurrentSecond() );
        }
    }
}