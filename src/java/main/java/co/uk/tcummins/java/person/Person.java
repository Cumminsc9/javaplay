package co.uk.tcummins.java.person;

public class Person
{
    private String firstName;
    private String secondName;
    private int age;

    public Person( final String firstName, final String secondName, final int age )
    {
        this.firstName = firstName;
        this.secondName = secondName;
        this.age = age;
    }


    public String getFirstName()
    {
        return firstName;
    }


    public void setFirstName( final String firstName )
    {
        this.firstName = firstName;
    }


    public String getSecondName()
    {
        return secondName;
    }


    public void setSecondName( final String secondName )
    {
        this.secondName = secondName;
    }


    public int getAge()
    {
        return age;
    }


    public void setAge( final int age )
    {
        this.age = age;
    }


    @Override
    public String toString()
    {
        return  "\nFirstName: "     + firstName  +
                "\nSecondName: "    + secondName +
                "\nAge: "           + age        +
                "\n----------------------------";
    }
}
