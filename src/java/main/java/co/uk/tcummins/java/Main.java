package co.uk.tcummins.java;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;


public class Main
{
    public static void main( String[] args )
    {

        try
        {

            String content = "foo";

            File file = new File( "highscore.txt" );



            FileWriter f = new FileWriter( file.getAbsoluteFile() );
            BufferedWriter b = new BufferedWriter( f );
            b.write( content + "\t" + "Score: " + "100" );

            b.close();

            System.out.println( "Score Saved" );

        }
        catch( IOException e )
        {
            e.printStackTrace();
        }
    }
}
