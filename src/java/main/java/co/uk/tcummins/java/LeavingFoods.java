package co.uk.tcummins.java;

public class LeavingFoods
{
    public static void main( String[] args )
    {
        final String[] responses = { "5:15", "5:15 pm", "dunno", "squirrel", "17:15", "6:00" };

        String rand1 = responses[((int) Math.floor( Math.random() * responses.length ))];

        System.out.println( rand1.equals( "17:15" ) );
    }
}