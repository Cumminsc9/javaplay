package co.uk.tcummins.java;

import java.util.Random;


public class RandomString
{
    private static Random rnd = new Random( 123456789 );


    public static void main( String[] args )
    {
        System.out.println( randomFRName0() + "-dummy-fr-0" );
        while ( true )
        {
            System.out.println( randomFRName1() + "-dummy-fr-0" );
        }
    }


    private static String randomFRName0()
    {
        final char[] alp = "abcdefghijklmnopqrstuvwxyzABCDEFJHIJKLMNOPQRSTUVWXYZ1234567890-=!@#$%^&*(){}:\"<>?".toCharArray();
        String newName = "";

        for( int i = 0; i < randomBetween( 5, 10 ); i++ )
        {
            newName = newName.concat( String.valueOf( alp[((int) (Math.random() * alp.length))] ) );
        }

        return newName;
    }


    private static String randomFRName1()
    {
        final String alp = "abcdefghijklmnopqrstuvwxyzABCDEFJHIJKLMNOPQRSTUVWXYZ1234567890-=!@#$%^&*(){}:\"<>?";
        final StringBuilder sb = new StringBuilder();

        for( int i = 0; i < randomBetween( 5, 10 ); i++ )
        {
            sb.append( alp.charAt( randomBetween( 0, ( alp.length()) ) ) );
        }

        return sb.toString();
    }


    private static int randomBetween( int low, int high )
    {
        return rnd.nextInt( high - low ) + low;
    }
}