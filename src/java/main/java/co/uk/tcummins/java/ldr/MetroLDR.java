package co.uk.tcummins.java.ldr;

import java.util.HashMap;
import java.util.Map;


public class MetroLDR
{
    private static Map<String, Integer> seriesList = new HashMap<String, Integer>();
    static
    {
        seriesList.put( "/com/intergral/fusionreactor/apm/cpu/all/instance", 100 );
        seriesList.put( "/com/intergral/fusionreactor/apm/cpu/all/total", 100 );
        seriesList.put( "/com/intergral/fusionreactor/apm/memory/heap/used", 512 );
        seriesList.put( "/com/intergral/fusionreactor/apm/memory/heap/allocated", 512 );
        seriesList.put( "/com/intergral/fusionreactor/apm/memory/heap/max", 512 );
        seriesList.put( "/transit/txntracker/JDBCRequest/active/activity", 145 );
        seriesList.put( "/transit/txntracker/JDBCRequest/active/time", 10000 );
        seriesList.put( "/transit/txntracker/JDBCRequest/history/activity", 85 );
        seriesList.put( "/transit/txntracker/JDBCRequest/history/time", 25000 );
        seriesList.put( "/transit/txntracker/WebRequest/active/activity", 78 );
        seriesList.put( "/transit/txntracker/WebRequest/active/time", 8000 );
        seriesList.put( "/transit/txntracker/WebRequest/history/activity", 50 );
        seriesList.put( "/transit/txntracker/WebRequest/history/time", 7000 );
    }

    public static void main( String[] args )
    {
        final Map<String, Object> response = new HashMap<String, Object>();

        for( Map.Entry<String, Integer> entry : seriesList.entrySet() )
        {
            response.put( "MetroLDR", info( entry.getKey(), entry.getValue() ) );
        }

        for (Map.Entry<String, Object> entry : response.entrySet())
        {
            String key = entry.getKey();
            Object value = entry.getValue();
            System.out.println( key + "\n" + value );
        }
    }

    private static Map info( final String key, final Integer value )
    {
        final Map<String, Object> info = new HashMap<String, Object>();

        info.put( key, data( key ) );

        return info;
    }

    private static Map data( final String id )
    {
        final Map<String, Object> data = new HashMap<String, Object>();

        data.put( "min", null );
        data.put( "id", id );
        data.put( "unit", "ms" );
        data.put( "aggregate", CreateMetrics.sortMetrics( id, 0 ) );
        data.put( "max", null );
        data.put( "name", "name" );
        data.put( "value", "1.0" );

        return data;
    }
}