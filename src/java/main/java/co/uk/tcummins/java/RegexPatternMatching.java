package co.uk.tcummins.java;

import co.uk.tcummins.helpers.ParseQuotes;

public class RegexPatternMatching
{
    private final static String str = "<spring:message code=\"common.about.team1\"/> " +
                                      "<br /> <br /> <spring:message code=\"common.about.team2\" /> " +
                                      "<br /> <br /> <spring:message code=\"common.about.team3\" /></p>";

    public static void main( String[] args )
    {
        ParseQuotes.parseStringQuotes( str ).forEach( System.out::println );
    }
}