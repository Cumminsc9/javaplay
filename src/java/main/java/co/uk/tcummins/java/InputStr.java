package co.uk.tcummins.java;

import co.uk.tcummins.helpers.StreamToString;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by tcummins on 15.03.16.
 * Project : JavaPlay
 */
public class InputStr
{
    private static final String FILE_PATH = "src/resources/test.txt";

    public static void main( String[] args )
    {
        try
        {
            InputStream inputStream = new FileInputStream( FILE_PATH );
            System.out.println(StreamToString.streamToString( inputStream ));
        }
        catch ( IOException e )
        {
            e.printStackTrace();
        }
    }
}