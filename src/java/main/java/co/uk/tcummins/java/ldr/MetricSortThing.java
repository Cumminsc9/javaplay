package co.uk.tcummins.java.ldr;

import java.util.HashMap;
import java.util.Map;

public class MetricSortThing
{
    public static void main( String[] args )
    {
        final Map<String, Integer> getMetrics = sortMetrics( "s" );
        final Integer value = getMetrics.get( "sum" ) == null ? 100 : getMetrics.get( "sum" ) / 60;

        System.out.println( "Without / " + getMetrics.get( "sum" ) );
        System.out.println( "With / " + value );
    }


    private static Map<String, Integer> sortMetrics( final String seriesID )
    {
        final Map<String, Integer> response = new HashMap<>();

        switch( seriesID )
        {
            case "s":
                response.put( "sum", 58000 );
                break;
            default:
                System.out.println( "Nothing under 'series'" );
                break;
        }

        return response;
    }
}