package co.uk.tcummins.java;

import co.uk.tcummins.helpers.Stopwatch;


public class Fibonacci
{
    public static void main( String[] args )
    {
        Stopwatch sw = new Stopwatch();

        int index = 0;
        while ( true )
        {
            sw.start();
            System.out.println( "Fibonacci Number: "    + fibonacciRecursion( index ) +
                                "\nMilliseconds: "      + sw.getElapsedTime() +
                                "\nSeconds: "           + sw.getElapsedTimeSecs() +
                                "\n-------------------------------------");
            index++;
            sw.stop();
        }
    }

    public static long fibonacciRecursion( int i )
    {
        if ( i == 0 )
            return 0;

        if( i <= 2 )
            return 1;

        return fibonacciRecursion( i - 1 ) + fibonacciRecursion( i - 2 );
    }
}