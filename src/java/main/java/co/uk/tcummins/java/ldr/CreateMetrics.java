package co.uk.tcummins.java.ldr;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * Not all fields are required for all metrics
 * All sums are divided by the count (generally 60) to get populate graphs in the cloud
 */
public final class CreateMetrics
{
    private final static Random rnd = new Random( 123456789 );


    public static Map<String, Object> sortMetrics( final String id, final int high )
    {
        Map<String, Object> metrics = new HashMap<>();
        metrics.clear();

        switch( id )
        {
            case "/com/intergral/fusionreactor/apm/cpu/all/total":
                metrics.put( "sum", randomBetween( 2500, 6000 ) );
                metrics.put( "count", 60 );
                break;
            case "/com/intergral/fusionreactor/apm/cpu/all/instance":
                metrics.put( "sum", randomBetween( 1000, 2000 ) );
                metrics.put( "count", 60 );
                break;
            case "/com/intergral/fusionreactor/apm/memory/heap/max":
                metrics.put( "sum", 58000 );
                metrics.put( "count", 60 );
                break;
            case "/com/intergral/fusionreactor/apm/memory/heap/allocated":
                metrics.put( "sum", randomBetween( 20000, 30000 ) );
                metrics.put( "count", 60 );
                break;
            case "/com/intergral/fusionreactor/apm/memory/heap/used":
                metrics.put( "sum", randomBetween( 10000, 15000 ) );
                metrics.put( "count", 60 );
                break;
            case "/transit/txntracker/JDBCRequest/active/activity":
                metrics.put( "sum", randomBetween( 1000, 8000 ) );
                metrics.put( "count", 60 );
                break;
            case "/transit/txntracker/JDBCRequest/active/time":
                metrics.put( "sum", randomBetween( 1000, 8000 ) );
                metrics.put( "count", 60 );
                break;
            case "/transit/txntracker/JDBCRequest/history/activity":
                metrics.put( "sum", randomBetween( 1000, 8000 ) );
                metrics.put( "count", 60 );
                break;
            case "/transit/txntracker/JDBCRequest/history/time":
                metrics.put( "sum", randomBetween( 1000, 8000 ) );
                metrics.put( "count", 60 );
                break;
            case "/transit/txntracker/WebRequest/active/activity":
                metrics.put( "sum", randomBetween( 1000, 8000 ) );
                metrics.put( "count", 60 );
                break;
            case "/transit/txntracker/WebRequest/active/time":
                metrics.put( "sum", randomBetween( 1000, 8000 ) );
                metrics.put( "count", 60 );
                break;
            case "/transit/txntracker/WebRequest/history/activity":
                metrics.put( "sum", randomBetween( 1000, 8000 ) );
                metrics.put( "count", 60 );
                break;
            case "/transit/txntracker/WebRequest/history/time":
                metrics.put( "sum", randomBetween( 1000, 8000 ) );
                metrics.put( "count", 60 );
                break;
            default:
                break;
        }

        return metrics;
    }


    private static int randomBetween( int low, int high )
    {
        return rnd.nextInt( Math.max( 1, high - low ) ) + low;
    }
}