package co.uk.tcummins.java.chain.of.responsibility;

/**
 * Created by tcummins on 03.03.16.
 * Project : JavaPlay
 */
public class HeaderTextProcessing extends ProcessingObject<String>
{

    @Override
    protected String handleWork( final String input )
    {
        return "Obi-Wan-Kenobi: You were like a brother Anakin! You were the chosen one. I trusted you! " + input;
    }
}