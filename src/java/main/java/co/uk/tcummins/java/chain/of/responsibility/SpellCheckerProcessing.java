package co.uk.tcummins.java.chain.of.responsibility;

/**
 * Created by tcummins on 03.03.16.
 * Project : JavaPlay
 */
public class SpellCheckerProcessing extends ProcessingObject<String>
{
    @Override
    protected String handleWork( final String input )
    {
        return input.replaceAll( "Little Annie", "Anakin" );
    }
}