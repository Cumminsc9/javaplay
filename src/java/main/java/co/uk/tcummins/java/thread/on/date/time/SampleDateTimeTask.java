package co.uk.tcummins.java.thread.on.date.time;

import java.util.TimerTask;


public class SampleDateTimeTask extends TimerTask
{
    Thread myThreadObj;

    SampleDateTimeTask( Thread t )
    {
        this.myThreadObj = t;
    }


    public void run()
    {
        myThreadObj.start();
    }
}
