package co.uk.tcummins.java.thread.on.date.time;

import co.uk.tcummins.helpers.RandomInteger;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
import java.util.Timer;


public class DateConverter
{
    private static String timeDate;
    private static String numAdd1;

    public static void main( String[] args )
    {
        Scanner scanner = new Scanner( System.in );

        System.out.println( "Enter date for when you want the task to start ( dd/MM/yyyy HH:mm:ss ) ");
        timeDate = scanner.nextLine();

        DateFormat userDate = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

        try
        {
            Date d2 = userDate.parse( timeDate );

            Timer timer = new Timer();
            Thread myThread = new Thread( new RunThis() );

            timer.schedule( new SampleDateTimeTask( myThread ), d2 );
        }
        catch ( ParseException e )
        {
            e.printStackTrace();
        }

        System.out.println("Now printing this. 1");
        System.out.println("Now printing this. 2");
        System.out.println("Now printing this. 3");
        System.out.println("Now printing this. 4");
        System.out.println("Now printing this. 5");

        System.out.println("Enter a number to be added :: ");
        numAdd1 = scanner.nextLine();

        System.out.println("Your number plus a random number are :: " + numAdd1 + RandomInteger.randomInt( 1, 50 ));

        System.out.println("Awaiting to start task ... ");
    }
}