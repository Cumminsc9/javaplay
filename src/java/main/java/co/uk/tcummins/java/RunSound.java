package co.uk.tcummins.java;

import java.io.File;
import java.util.concurrent.TimeUnit;

import javax.sound.sampled.*;

/**
 * http://www.codejava.net/coding/how-to-play-back-audio-in-java-with-examples
 */
public class RunSound implements LineListener
{
    private boolean isPlayFinished;


    public static void main( String[] args )
    {
        final String audioDirectory = "src/resources/sounds/absolute_end.wav";

        RunSound runSound = new RunSound();
        runSound.play( audioDirectory );
    }


    private void play( final String audioPath )
    {
        final File audioFile = new File( audioPath );

        try
        {
            AudioInputStream audioInputStream = AudioSystem.getAudioInputStream( audioFile );
            AudioFormat audioFormat = audioInputStream.getFormat();

            DataLine.Info info = new DataLine.Info( Clip.class, audioFormat );

            Clip audioClip = (Clip) AudioSystem.getLine( info );
            audioClip.addLineListener( this );
            audioClip.open( audioInputStream );
            audioClip.start();

            while( !isPlayFinished )
            {
                Thread.sleep( 1000 );
            }

            audioClip.close();

            System.out.println( "Clip length: " + TimeUnit.MICROSECONDS.toSeconds( audioClip.getMicrosecondLength() ) + "secs" );
        }
        catch( Exception e )
        {
            e.printStackTrace();
        }
    }


    @Override
    public void update( final LineEvent event )
    {
        final LineEvent.Type type = event.getType();

        if( type == LineEvent.Type.START )
        {
            System.out.println( "Playback Started..." );
        }

        if( type == LineEvent.Type.STOP )
        {
            isPlayFinished = true;
            System.out.println( "Playback Completed..." );
        }
    }
}