package co.uk.tcummins.java.person;

import java.util.ArrayList;
import java.util.List;


public class PersonMain
{
    public static void main( String[] args )
    {
        final String[] fNames = { "Tom", "Mikey", "John", "Bernd", "Scott", "Ben", "Ben", "Terry", "Josh", "Neil", "Darren",
                "David" };

        final String[] sNames = { "Cummins", "Flewitt", "Hawksley", "Donath", "Spittle", "Everett", "Donnelly", "Pywell",
                "Wightman", "Tattersall", "Wilkinson", "Turner" };


        List<Person> listPeople = new ArrayList<Person>();

        for( int i = 0; i < 1; i++ )
        {
            String rand1 = fNames[((int) Math.floor( Math.random() * fNames.length ))];
            String rand2 = sNames[((int) Math.floor( Math.random() * sNames.length ))];

            int rand3 = 20 + (int) (Math.random() * ((50 - 19) + 1));

            Person p = new Person( rand1, rand2, rand3 );
            listPeople.add( p );
        }

        for( Person p : listPeople )
        {
            System.out.println( "ID: " + (int) (Math.random() * 55) + p.toString() );
        }
    }
}
