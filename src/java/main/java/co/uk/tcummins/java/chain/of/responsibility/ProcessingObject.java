package co.uk.tcummins.java.chain.of.responsibility;

/**
 * Created by tcummins on 03.03.16.
 * Project : JavaPlay
 */
public abstract class ProcessingObject<T>
{
    protected ProcessingObject<T> successor;

    public void setSuccessor( ProcessingObject successor )
    {
        this.successor = successor;
    }

    public T handle( T input )
    {
        T r = handleWork( input );

        if( successor != null )
        {
            return successor.handle( r );
        }

        return r;
    }

    abstract protected T handleWork( T input );
}