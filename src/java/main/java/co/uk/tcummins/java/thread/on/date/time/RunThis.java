package co.uk.tcummins.java.thread.on.date.time;

public class RunThis implements Runnable
{
    @Override
    public void run()
    {
        try
        {
            Thread.sleep( 500 );
            System.out.println( "Starting task on user inputted date now");
            while ( true )
            {
                System.out.println( "Running this task" );
            }
        }
        catch ( InterruptedException e )
        {
            e.printStackTrace();
        }
    }
}
