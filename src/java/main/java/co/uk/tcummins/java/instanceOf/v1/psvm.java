package co.uk.tcummins.java.instanceOf.v1;

public class psvm
{
    public static void main( String[] args )
    {
        Animal dog = new Dog();
        Animal animal = new Animal();

        System.out.println( dog instanceof IDomestic );

        System.out.println( dog instanceof Animal );

        System.out.println( dog instanceof Dog );

        System.out.println( dog instanceof Object );

        System.out.println( animal instanceof Dog );

        System.out.println( animal instanceof Cat );
    }
}

/**
 * 'instanceof' keyword is a binary operator used to test if an object (instance) is a subtype of a given Type.
 *
 *   Imagine:
 *
 *   interface IDomestic {}
 *   class Animal {}
 *   class Dog extends Animal implements IDomestic {}
 *   class Cat extends Animal implements IDomestic {}
 *   Imagine a dog object, created with Object dog = new Dog(), then:
 *
 *   dog instanceof IDomestic // true - Dog implements IDomestic
 *   dog instanceof Animal   // true - Dog extends Animal
 *   dog instanceof Dog      // true - Dog is Dog
 *   dog instanceof Object   // true - Object is the parent type of all objects
 *   However,
 *
 *   animal instanceof Dog // false
 *   because Animal is a supertype of Dog and possibly less "refined".
 *
 *   And,
 *
 *   dog instanceof Cat // does not even compile!
 *   This is because Dog is neither a subtype nor a supertype of Cat, and it also does not implement it.
 *
 *   Note that the variable used for dog above is of type Object. This is to show instanceof is a runtime operation and brings us to a/the use case: to react differently based upon an objects type at runtime.
 *
 *   Things to note: expressionThatIsNull instanceof T is false for all Types T.
 */