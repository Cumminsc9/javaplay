package co.uk.tcummins.java;

import java.util.ArrayList;


/**
 * Created by tcummins on 17.03.16.
 * Project : JavaPlay
 */
public class BalancedParaenthesis
{
    public static void main( String[] args )
    {
        String str = "{[(])}(){}";
        int pairs = 0;
        boolean unableToFind = false;
        ArrayList<Character> anChar = new ArrayList<Character>();

        for( int i = 0; i < str.length(); i++ )
        {
            anChar.add( str.charAt( i ) );
        }

        if( str.length() % 2 == 0 )
        {
            while( pairs != str.length() / 2 )
            {
                for( int i = 1; i < anChar.size(); i++ )
                {
                    char a = (char) anChar.get( 0 );
                    char b = (char) anChar.get( i );
                    if( a == '{' && b == '}' || a == '[' && b == ']' || a == '(' && b == ')' )
                    {
                        anChar.remove( i );
                        anChar.remove( 0 );
                        pairs++;
                        break;
                    }
                    else
                    {
                        if( i == anChar.size() - 1 )
                        { // reached end of array
                            unableToFind = true;
                            break;
                        }
                    }
                }
                if( unableToFind )
                    break;
            }

            if( pairs == str.length() / 2 )
            {
                System.out.println( "Log#01: The String have balanced parenthesis" );
            }
            else
            {
                System.out.println( "Log#02: The String do not have balanced parenthesis. (" + pairs + "/" + str.length() / 2
                                    + " pairs found)" );
            }
        }
        else
        {
            System.out.println( "Log#03: The String do not have even numbers of parenthesis" );
        }
    }
}
