package co.uk.tcummins.java.chain.of.responsibility;

import java.util.function.Function;
import java.util.function.UnaryOperator;


/**
 * Created by tcummins on 03.03.16.
 * Project : JavaPlay
 */
public class Main
{
    public static void main( String[] args )
    {
        ProcessingObject<String> p1 = new HeaderTextProcessing();
        ProcessingObject<String> p2 = new SpellCheckerProcessing();

        /** Chaining two processing objects */
        p1.setSuccessor( p2 );

        String result = p1.handle( "Little Annie: I HATE YOU!!" );
        System.out.println( result );

        System.out.println( chaining() );
    }


    /**
     * Chaining, ( that is composing ) functions
     *
     * You can represent the processing objects as an instance of
     * Function<String, String> or more precisely a UnaryOperator<String>.
     * To chain them you just need to compose these functions by using the andThen method!
     *
     * @return
     */
    private static String chaining()
    {
        /** The first processing object */
        UnaryOperator<String> headerProcessing = ( String text ) -> "Obi-Wan-Kenobi: You were like a brother Anakin! You were the chosen one. I trusted you! " + text;

        /** The second processing object */
        UnaryOperator<String> spellCheckingProcessing = ( String text ) -> text.replaceAll( "Little Annie", "Anakin" );

        /** Compose the two functions, resulting in a chain of operations */
        Function<String, String> pipeline = headerProcessing.andThen( spellCheckingProcessing );

        return pipeline.apply( "Little Annie: I HATE YOU!!" );
    }
}