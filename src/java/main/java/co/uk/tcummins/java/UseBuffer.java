package co.uk.tcummins.java;

import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by tcummins on 24.03.16.
 */
public class UseBuffer
{

    public static void main( String[] args ) throws InterruptedException
    {
        Scanner scanner = new Scanner( System.in );

        System.out.println( "Enter seconds: " );
        int seconds = Integer.parseInt( scanner.next() );

        System.out.println( "Enter memory: " );
        int mb = Integer.parseInt( scanner.next() );

        final int buffSize = mb * 1024 * 1024;

        System.out.println( "Consuming " + mb + "MB" );
        System.out.flush();

        final long finishTime = System.currentTimeMillis() + (seconds * 1000);
        final List<Buffer> buffers = new ArrayList<>();

        while( System.currentTimeMillis() < finishTime )
        {
            buffers.add( ByteBuffer.allocateDirect( buffSize ) );
            Thread.sleep(1000);
        }
    }
}
