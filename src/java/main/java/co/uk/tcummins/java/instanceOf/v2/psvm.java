package co.uk.tcummins.java.instanceOf.v2;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class psvm
{
    public static void main( String[] args )
    {
        String s0 = "Hello World!";
        System.out.println( s0 instanceof String ); //result --> true

        String s1 = null;
        System.out.println( s1 instanceof String ); //result --> false

        //Since a subclass 'is a' type of its superclass, the following if statement, where Child is a subclass of Parent, returns true.
        Child child = new Child();
        System.out.println( child instanceof Parent ); //result --> true

        Map m = new HashMap();
        System.out.println( "Map instanceof Map " + (m instanceof Map) );      //Returns a boolean value true
        System.out.println( "Map instanceof HashMap " + (m instanceof HashMap) );  //Returns a boolean value true
        System.out.println( "Map instanceof Object " + (m instanceof Object) );   //Returns a boolean value true
        System.out.println( "Map instanceof Date " + (m instanceof Date) );     //Returns a boolean value false
    }
}



class Parent
{
    public Parent()
    {

    }
}



class Child extends Parent
{
    public Child()
    {
        super();
    }
}