package co.uk.tcummins.java;

import org.omg.Messaging.SYNC_WITH_TRANSPORT;

import java.util.Arrays;
import java.util.List;

import static java.util.stream.Collectors.toList;


/**
 * Created by tcummins on 03.03.16.
 * Project : JavaPlay
 */
public class Debugging
{
    public static void main( String[] args )
    {
        List<Integer> nums = Arrays.asList( 2, 3, 4, 5 );

        //nums.stream().map( Debugging::divideByZero ).forEach( System.out::println );

        //nums.stream().map( x -> x + 17 ).filter( x -> x % 2 == 0 ).limit( 3 ).forEach( System.out::println );

        nums.stream()
            .peek( x -> System.out.println( "from stream: " + x ) )
            .map( x -> x + 17 )
            .peek( x -> System.out.println( "after map: " + x ) )
            .filter( x -> x % 2 == 0 )
            .peek( x -> System.out.println( "after filter: " + x ) )
            .limit( 3 ).peek( x -> System.out.println( "after limit: " + x) )
            .collect( toList() ) ;
    }

    private static int divideByZero( final int n )
    {
        return n / 0;
    }
}