package co.uk.tcummins.java;

import co.uk.tcummins.helpers.StreamToString;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;


/**
 * Created by tcummins on 03.03.16.
 * Project : JavaPlay
 */
public class RandomStreams
{
    public static void main( String[] args )
    {
        Stream<double[]> pythTriple = IntStream.rangeClosed( 1, 100 )
                                               .boxed().limit( 1 )
                                               .flatMap( a -> IntStream.rangeClosed( a, 100 )
                                                                       .mapToObj( b -> new double[]{ a, b, Math.sqrt(
                                                                               a * a + b + b ) } )
                                                                       .filter( t -> t[2] % 1 == 0 ) );

        //https://blog.codecentric.de/en/2013/10/java-8-first-steps-lambdas-streams/
        pythTriple.forEach( doubles -> {
            for ( double aDouble : doubles )
            {
                System.out.println( aDouble );
            }
        } );

        //==============================================================================================================
            System.out.println( "" );
        //==============================================================================================================

        List<Integer> someNumbers = Arrays.asList( 1, 2, 3, 4, 5 );
        Optional<Integer> firstSquareDivisibleByThree = someNumbers.stream()
                                                                   .map( x -> x * x )
                                                                   .filter( x -> x % 3 == 0 )
                                                                   .findFirst(); // 9
        System.out.println( firstSquareDivisibleByThree );

        //==============================================================================================================
            System.out.println( "" );
        //==============================================================================================================

        List<Integer> numbers = Arrays.asList( 1, 2, 1, 3, 3, 2, 4 );
        numbers.stream().filter( i -> i % 2 == 0 ).distinct().forEach( System.out::println );

        //==============================================================================================================
            System.out.println( "" );
        //==============================================================================================================

        List<String> helloWorld = Arrays.asList( "Hello", "World" );
        String[] goodByeWorld ={ "Goodbye", "World" };

        Stream<String> stringStream = Arrays.stream( goodByeWorld );
        List<String> uniqueChars = helloWorld.stream()
                                             .map( w -> w.split( "" ) )
                                             .flatMap( Arrays::stream )
                                             .distinct()
                                             .collect( toList() );

        stringStream.forEach( System.out::println );
        uniqueChars.forEach( System.out::println );

        //==============================================================================================================
            System.out.println( "" );
        //==============================================================================================================

        List<String> title = Arrays.asList( "Java8", "In", "Action" );
        Stream<String> s = title.stream();
        s.forEach( System.out::println );
    }
}