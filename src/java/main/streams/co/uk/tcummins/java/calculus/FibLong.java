package co.uk.tcummins.java.calculus;

import java.util.function.LongSupplier;
import java.util.stream.LongStream;


/**
 * Created by tcummins on 02.03.16.
 * Project : JavaPlay
 */
public class FibLong
{
    public static void main( String[] args )
    {
        LongSupplier fib = new LongSupplier()
        {
            private long previous = 0L;
            private long current = 1L;

            public long getAsLong()
            {
                long oldPrevious = this.previous;
                long nextValue = this.previous + this.current;
                this.previous = this.current;
                this.current = nextValue;
                return oldPrevious;
            }
        };
        LongStream.generate( fib ).limit( 60 ).forEach( System.out::println );
    }
}