package co.uk.tcummins.java.calculus.prime.number;

import static java.util.stream.Collector.Characteristics.IDENTITY_FINISH;

import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;

/**
 * Created by tcummins on 02.03.16.
 * Project : JavaPlay
 */
public class PrimeNumberCollector implements Collector<Integer,
                                                    Map<Boolean, List<Integer>>,
                                                    Map<Boolean, List<Integer>>>
{
    /**
     * Start the collection process with a Map containing two empty Lists
     *
     * @return
     */
    @Override
    public Supplier<Map<Boolean, List<Integer>>> supplier()
    {
        return () -> new HashMap<Boolean, List<Integer>>()
        {
            {
                put( true, new ArrayList<>() );
                put( false, new ArrayList<>() );
            }
        };
    }


    /**
     * Pass to the isPrime method the list of already found primes
     *
     * Get from the Map the list of prime of non-prime numbers according to what the isPrime method
     * returned, and add it to the current candidate
     *
     * @return
     */
    @Override
    public BiConsumer<Map<Boolean, List<Integer>>, Integer> accumulator()
    {
        return ( Map<Boolean, List<Integer>> acc, Integer candidate ) ->
            acc.get( isPrime( acc.get( true ), candidate ) ).add( candidate );
    }

    /**
     * Merge the second Map into the first one
     *
     * @return
     */
    @Override
    public BinaryOperator<Map<Boolean, List<Integer>>> combiner()
    {
        return ( Map<Boolean, List<Integer>> map1,
                 Map<Boolean, List<Integer>> map2 ) ->
        {
            map1.get( true ).addAll( map2.get( true ) );
            map2.get( false ).addAll( map2.get( false ) );
            return map1;
        };
    }


    /**
     * No transformation is necessary at the end of the collection process, so terminate it with the
     * identity function
     *
     * @return
     */
    @Override
    public Function<Map<Boolean, List<Integer>>, Map<Boolean, List<Integer>>> finisher()
    {
        return Function.identity();
    }


    /**
     * This collector is IDENTITY_FINISH but neither UNORDERED nor CONCURRENT because it relies on
     * the fact that prime numbers are discovered in sequence
     *
     * @return
     */
    @Override
    public Set<Characteristics> characteristics()
    {
        return Collections.unmodifiableSet( EnumSet.of( IDENTITY_FINISH ) );
    }


    /**
     * Generate a range of natural numbers starting from and including 2 up to but excluding candidate
     *
     * @param candidate
     * @return 'true' if the candidate isn't divisible for any of the numbers in the stream
     */
    public boolean isPrime(List<Integer> primes, int candidate)
    {
        return primes.stream().noneMatch(i -> candidate % i == 0);
    }
}