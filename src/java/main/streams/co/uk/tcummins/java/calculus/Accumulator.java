package co.uk.tcummins.java.calculus;

/**
 * Created by tcummins on 03.03.16.
 * Project : JavaPlay
 */
public class Accumulator
{
    public long total = 0;

    public void add( long value )
    {
        total += value;
    }
}
