package co.uk.tcummins.java.strings;

/**
 * Created by tcummins on 03.03.16.
 * Project : JavaPlay
 */
public class WordCount
{
    private final int counter;
    private final boolean lastSpace;


    public WordCount( final int counter, final boolean lastSpace )
    {
        this.counter = counter;
        this.lastSpace = lastSpace;
    }


    /**
     * The accumlate method traverses the Characters one by one
     * as done by the iterative algorithm
     *
     * @param c
     * @return
     */
    public WordCount accumluate( Character c )
    {
        if( Character.isWhitespace( c ) )
        {
            return lastSpace ? this : new WordCount( counter, true );
        }
        else
        {
            /** Increase the word counter when the last character is a space and the currently traversed one isn't */
            return lastSpace ? new WordCount( counter+1, false ) : this;
        }
    }


    /**
     * Combine two WordCounts by summing their counters
     *
     * @param wordCount
     * @return
     */
    public WordCount combine( WordCount wordCount )
    {
        /** Use only the sum of the counters so you don't care about lastSpace */
        return new WordCount( counter + wordCount.counter, wordCount.lastSpace );
    }

    public int getCounter()
    {
        return counter;
    }
}
