package co.uk.tcummins.java.calculus.parallel.sum;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.function.Function;
import java.util.stream.LongStream;


/**
 * Created by tcummins on 03.03.16.
 * Project : JavaPlay
 */
public class Main
{
    public static void main( String[] args )
    {
        System.out.println("ForkJoin sum done in: " + measureSumPerf( Main::forkJoinSum, 10_000_000) + " msecs" );
    }

    private static long forkJoinSum(long n)
    {
        long[] numbers = LongStream.rangeClosed( 1, n ).toArray();
        ForkJoinTask<Long> task = new ForkJoinSumCalculator(numbers);
        return new ForkJoinPool().invoke(task);
    }

    private static long measureSumPerf(Function<Long, Long> adder, long n)
    {
        long fastest = Long.MAX_VALUE;
        for (int i = 0; i < 1; i++)
        {
            long start = System.nanoTime();
            long sum = adder.apply(n);
            long duration = (System.nanoTime() - start) / 1_000_000;

            System.out.println("Result: " + sum);

            if (duration < fastest)
                fastest = duration;
        }

        return fastest;
    }
}
