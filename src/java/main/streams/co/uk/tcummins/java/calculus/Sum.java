package co.uk.tcummins.java.calculus;

import java.util.function.Function;
import java.util.stream.LongStream;
import java.util.stream.Stream;


/**
 * Created by tcummins on 03.03.16. Project : JavaPlay
 */
public class Sum
{
    public static void main( String[] args )
    {
        //System.out.println("Iterative sum done in: " + measureSumPerf(Sum::iterativeSum, 10_000_000) + " msecs");

        //System.out.println("Sequential sum done in: " + measureSumPerf(Sum::parallelSum, 10_000_000) + " msecs");

        //System.out.println("Ranged sum done in: " + measureSumPerf(Sum::rangedSum, 10_000_000) + " msecs");

        //System.out.println("Parallel Ranged sum done in: " + measureSumPerf(Sum::parallelRangedSum, 10_000_000) + " msecs");

        System.out.println("SideEffect sum done in: " + measureSumPerf( Sum::sideEffectSum, 10_000_000L ) + " msecs" );

        System.out.println("SideEffect parallel sum done in: " + measureSumPerf( Sum::sideEffectParallelSum, 10_000_000L ) + " msecs" );
    }


    private static Long iterativeSum( final long n )
    {
        long result = 0;

        for ( long i = 0; i < n ; i++ )
        {
            result += 1;
        }

        return result;
    }


    private static long parallelSum( final long n )
    {
        return Stream.iterate( 1L, i -> i + 1 ).parallel().limit( n ).reduce( 0L, Long::sum );
    }


    public static long rangedSum( long n )
    {
        return LongStream.rangeClosed( 1, n ).reduce( 0L, Long::sum );
    }


    public static long parallelRangedSum( long n )
    {
        return LongStream.rangeClosed(1, n).parallel().reduce( 0L, Long::sum);
    }


    public static long sideEffectSum( long n )
    {
        Accumulator accumulator = new Accumulator();
        LongStream.rangeClosed(1, n).forEach(accumulator::add);

        return accumulator.total;
    }


    /**
     * This is caused by the fact that multiple threads are concurrently accessing the
     * accumulator and in particular executing total += value, which, despite
     * its appearance, isn’t an atomic operation.
     *
     * @param n
     * @return a different result each time distant from the correct value of 50000005000000
     */
    public static long sideEffectParallelSum(long n)
    {
        Accumulator accumulator = new Accumulator();
        LongStream.rangeClosed(1, n).parallel().forEach(accumulator::add);
        return accumulator.total;
    }


    private static long measureSumPerf(Function<Long, Long> adder, long n)
    {
        long fastest = Long.MAX_VALUE;
        for (int i = 0; i < 10; i++)
        {
            long start = System.nanoTime();
            long sum = adder.apply(n);
            long duration = (System.nanoTime() - start) / 1_000_000;

            System.out.println("Result: " + sum);

            if (duration < fastest)
                fastest = duration;
        }

        return fastest;
    }
}