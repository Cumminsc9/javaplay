package co.uk.tcummins.java.tradtrans;

import java.lang.management.OperatingSystemMXBean;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Currency;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.toList;


/**
 * Created by tcummins on 02.03.16.
 * Project : JavaPlay
 */
public class Main
{
    public static void main( String[] args )
    {
        Trader raoul = new Trader("Raoul", "Cambridge");
        Trader mario = new Trader("Mario","Milan");
        Trader alan = new Trader("Alan","Cambridge");
        Trader brian = new Trader("Brian","Cambridge");

        List<Transaction> transactions = Arrays.asList(
                new Transaction( brian, 2011, 300 ),
                new Transaction( raoul, 2012, 1000 ),
                new Transaction( raoul, 2011, 400 ),
                new Transaction( mario, 2012, 710 ),
                new Transaction( mario, 2012, 700 ),
                new Transaction( alan, 2012, 950 )
        );

        List<Transaction> tr2011 = transactions.stream()
                                              .filter( transaction -> transaction.getYear() == 2011 )
                                              .sorted( comparing( Transaction::getValue ) )
                                              .collect( toList() );

        List<String> cities = transactions.stream()
                                          .map( transaction -> transaction.getTrader().getCity() ) // extract the city from each trader bound with the transaction
                                          .distinct() // select only unique cities
                                          .collect( toList() );

        List<Trader> traders = transactions.stream()
                                           .map( Transaction::getTrader )
                                           .filter( trader -> trader.getCity().equals( "Cambridge" ) )
                                           .distinct() // make sure there are no duplicates
                                           .sorted( comparing( Trader::getName ) ) // sort by name
                                           .collect( toList() );

        String traderStr = transactions.stream()
                                       .map( transaction -> transaction.getTrader().getName() )
                                       .distinct() //select only the unique names
                                       .sorted() // sort names alphabetically
                                       .reduce( "", ( n1, n2 ) -> n1 + n2 ); //combine each name one by one to form a String that concats all the names

        boolean milanBased = transactions.stream()
                                         .anyMatch( transaction -> transaction.getTrader().getCity().equals( "Milan" ) );

        transactions.stream().filter( t -> "Cambridge".equals( t.getTrader().getCity() ) )
                    .map( Transaction::getValue )
                    .forEach( System.out::println );

        Optional<Integer> highestValue = transactions.stream()
                                                     .map( Transaction::getValue )
                                                     .reduce( Integer::max );

        Optional<Transaction> smallestTrans = transactions.stream()
                                                          .reduce( (t1, t2) -> t1.getValue() < t2.getValue() ? t1 : t2);

    }
}
