package co.uk.tcummins.java.dish;

import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.mapping;
import static java.util.stream.Collectors.maxBy;
import static java.util.stream.Collectors.partitioningBy;
import static java.util.stream.Collectors.toCollection;
import static java.util.stream.Collectors.toList;

import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;


/**
 * Created by tcummins on 02.03.16.
 * Project : JavaPlay
 */
public class DishStreams
{
    public static void main( String[] args )
    {
        List<Dish> menu = Arrays.asList( new Dish( "pork", false, 800, Dish.Type.MEAT ),
                                         new Dish( "beef", false, 700, Dish.Type.MEAT ),
                                         new Dish( "chicken", false, 400, Dish.Type.MEAT ),
                                         new Dish( "french fries", true, 530, Dish.Type.OTHER ),
                                         new Dish( "rice", true, 350, Dish.Type.OTHER ),
                                         new Dish( "season fruit", true, 120, Dish.Type.OTHER ),
                                         new Dish( "pizza", true, 550, Dish.Type.OTHER ),
                                         new Dish( "prawns", false, 300, Dish.Type.FISH ),
                                         new Dish( "salmon", false, 450, Dish.Type.FISH ) );

        /** dish::getName, is equivalent to the lambda d -> d.getName(). */
        List<String> highCalorieDishes = menu.stream()      /** get a stream from 'menu' */
                .filter( d -> d.getCalories() > 300 )       /** create a pipeline of operation: first filter, high-calorie dishes */
                .limit( 3 )                                 /** limits the result set to '3' */
                .map( Dish::getName )                       /** get the names of the dishes */
                .collect( toList() );                       /** store the results in the new list 'highCalorieDishes' */

        highCalorieDishes.forEach( System.out::println );   /** prints the list 'highCalorieDishes' */

        //==============================================================================================================
            System.out.println( highCalorieDishes.getClass().getName() );
        //==============================================================================================================

        List<String> names = menu.stream()
                                 .filter( d ->
                                          {
                                              System.out.println( "filtering " + d.getName() );
                                              return d.getCalories() > 300;
                                          } )
                                 .map( d ->
                                       {
                                           System.out.println( "mapping" + d.getName() );
                                           return d.getName();
                                       } )
                                 .limit( 3 )
                                 .collect( toList() );

        names.forEach( System.out::println );

        //==============================================================================================================
            System.out.println( "" );
        //==============================================================================================================

        List<Dish> vegetarianDishes = menu.stream().filter( Dish::isVegetarian ).collect( toList() );
        vegetarianDishes.forEach( System.out::println );

        //==============================================================================================================
            System.out.println( "" );
        //==============================================================================================================

        List<Dish> dishes = menu.stream()
                                .filter(d -> d.getCalories() > 300)
                                .skip(2)
                                .collect(toList());

        dishes.forEach( System.out::println );

        //==============================================================================================================
            System.out.println( "" );
        //==============================================================================================================

        if( menu.stream().anyMatch( Dish::isVegetarian ) )
        {
            System.out.println("The menu is (somewhat) vegetarian friendly!!");
        }

        //==============================================================================================================
            System.out.println( "" );
        //==============================================================================================================

        Map<Dish.CaloricLevel, List<Dish>> dishesByCaloricLevel = menu.stream()
                                                                      .collect( groupingBy( dish ->
                                                                                            {
                                                                                                if ( dish.getCalories() <= 400 )
                                                                                                    return Dish.CaloricLevel.DIET;
                                                                                                else if (
                                                                                                        dish.getCalories() <= 700 )
                                                                                                    return Dish.CaloricLevel.NORMAL;
                                                                                                else
                                                                                                    return Dish.CaloricLevel.FAT;
                                                                                            } ) );
        printMaps( dishesByCaloricLevel.entrySet().iterator() );

        //==============================================================================================================
            System.out.println( "" );
        //==============================================================================================================

        Map<Dish.Type, List<Dish>> dishesByType = menu.stream().collect(groupingBy(Dish::getType));

        printMaps( dishesByType.entrySet().iterator() );

        //==============================================================================================================
            System.out.println( "" );
        //==============================================================================================================

        Map<Dish.Type, Map<Dish.CaloricLevel, List<Dish>>> dishesByTypeCaloricLevel
                                                            = menu.stream()
                                                                  .collect( groupingBy(
                                                                          Dish::getType,
                                                                          groupingBy( dish ->
                                                                                      {
                                                                                          if ( dish.getCalories() <= 400 )
                                                                                              return Dish.CaloricLevel.DIET;
                                                                                          else if (
                                                                                                  dish.getCalories() <= 700 )
                                                                                              return Dish.CaloricLevel.NORMAL;
                                                                                          else
                                                                                              return Dish.CaloricLevel.FAT;
                                                                                      } ) ) );
        printMaps( dishesByTypeCaloricLevel.entrySet().iterator() );

        //==============================================================================================================
            System.out.println( "" );
        //==============================================================================================================

        Map<Dish.Type, Dish> mostCaloricByType = menu.stream().collect( groupingBy( Dish::getType, collectingAndThen( maxBy(
                Comparator.comparingInt(Dish::getCalories) ), Optional::get ) ) );

        printMaps( mostCaloricByType.entrySet().iterator() );

        //==============================================================================================================
            System.out.println( "" );
        //==============================================================================================================

        Map<Dish.Type, Set<Dish.CaloricLevel>> caloricLevelsByType
                                                = menu.stream()
                                                      .collect( groupingBy(
                                                              Dish::getType,
                                                              mapping( dish ->
                                                                       {
                                                                           if ( dish.getCalories() <= 400 )
                                                                               return Dish.CaloricLevel.DIET;
                                                                           else if ( dish.getCalories() <= 700 )
                                                                               return Dish.CaloricLevel.NORMAL;
                                                                           else
                                                                               return Dish.CaloricLevel.FAT;
                                                                       },
                                                                       toCollection( HashSet::new ) ) ) );

        printMaps( caloricLevelsByType.entrySet().iterator() );

        //==============================================================================================================
            System.out.println( "" );
        //==============================================================================================================

        Map<Boolean, List<Dish>> partitionMenu = menu.stream().collect( partitioningBy( Dish::isVegetarian ) );

        printMaps( partitionMenu.entrySet().iterator() );

        //==============================================================================================================
            System.out.println( "" );
        //==============================================================================================================

        Map<Boolean, Map<Dish.Type, List<Dish>>> vegDishesByTYpe = menu.stream()
                                                                       .collect(
                                                                               partitioningBy( Dish::isVegetarian,
                                                                                               groupingBy(
                                                                                                       Dish::getType ) ) );
        printMaps( vegDishesByTYpe.entrySet().iterator() );

        printMap( vegDishesByTYpe );

        //==============================================================================================================
            System.out.println( "" );
        //==============================================================================================================

        List<Dish> dishs = menu.stream().collect( new ToListCollector<Dish>() );

        dishs.forEach( System.out::println );

        //==============================================================================================================
            System.out.println( "" );
        //==============================================================================================================

    }


    /**
     * Pass in the maps iterator, and use a while
     * loop to get all entries in the map
     *
     * Call Example : printMaps( mapName.entrySet().iterator() );
     *
     * @param iterator The maps iterator is passed in when the method is called
     */
    private static void printMaps( Iterator iterator )
    {
        while( iterator.hasNext() )
        {
            Map.Entry entry = ( Map.Entry ) iterator.next();

            System.out.println( entry.getKey() );
            System.out.println( entry.getValue() );
        }
    }


    /**
     * Pass in the map and use a forEach loop to get
     * all entries in the map
     *
     * Call Example : printMap( mapName );
     *
     * @param map The map that is passed in when the method is called
     */
    private static void printMap( Map<?, ?> map )
    {
        for( Map.Entry<?, ?> entry : map.entrySet() )
        {
            System.out.println( entry.getKey() );
            System.out.println( entry.getValue() );
        }
    }
}