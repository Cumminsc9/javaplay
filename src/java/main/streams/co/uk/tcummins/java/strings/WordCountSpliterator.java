package co.uk.tcummins.java.strings;

import java.util.Spliterator;
import java.util.function.Consumer;


/**
 * Created by tcummins on 03.03.16.
 * Project : JavaPlay
 */
public class WordCountSpliterator implements Spliterator<Character>
{
    private final String string;
    private int currentChar = 0;


    public WordCountSpliterator( final String string )
    {
        this.string = string;
    }


    /**
     * @param action
     * @return true if there are further characters to be consumed
     */
    @Override
    public boolean tryAdvance( final Consumer<? super Character> action )
    {
        /** Consume the current character */
        action.accept( string.charAt( currentChar++ ) );

        return currentChar < string.length();
    }


    @Override
    public Spliterator<Character> trySplit()
    {
        int currentSize = string.length() - currentChar;
        if( currentSize < 10 )
        {
            /** Return null to signal that the String to be parsed in small enough to be processed sequentially */
            return null;
        }

        /** Set the candidates split position to be half of the String to be parsed */
        for(int splitPos = currentSize / 2 + currentChar; splitPos < string.length(); splitPos++)
        {
            /** Advance the split position until the next space */
            if( Character.isWhitespace( string.charAt( splitPos ) ) )
            {
                /** Create a new WordCountSpliterator parsing the String from the start to the split position */
                Spliterator<Character> spliterator = new WordCountSpliterator( string.substring( currentChar, splitPos ) );

                /** Set the start position of this WordCountSpliterator to the split position */
                currentChar = splitPos;
                return spliterator;
            }
        }

        return null;
    }


    @Override
    public long estimateSize()
    {
        return string.length() - currentChar;
    }


    @Override
    public int characteristics()
    {
        return ORDERED + SIZED + SUBSIZED + NONNULL + IMMUTABLE;
    }
}