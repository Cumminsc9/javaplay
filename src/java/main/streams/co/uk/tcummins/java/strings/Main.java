package co.uk.tcummins.java.strings;

import java.util.Spliterator;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;


/**
 * Created by tcummins on 03.03.16.
 * Project : JavaPlay
 */
public class Main
{
    private static final String SENTENCE = " Nel   mezzo del cammin  di nostra  vita mi  ritrovai in una  selva oscura" +
                                           " ché la  dritta via era   smarrita ";

    public static void main( String[] args )
    {
        System.out.println("Found " + countWordsIteratively( SENTENCE ) + " words");

        Stream<Character> stream0 = IntStream.range( 0, SENTENCE.length() ).mapToObj( SENTENCE::charAt );
        System.out.println("Found " + countWords(stream0) + " words");

        Spliterator<Character> spliterator = new WordCountSpliterator(SENTENCE);
        Stream<Character> stream1 = StreamSupport.stream( spliterator, true );
        System.out.println("Found " + countWords(stream1) + " words");
    }



    private static int countWordsIteratively( final String s )
    {
        int count = 0;
        boolean lastSpace = true;

        /** Traverse all the characters in the String one by one */
        for ( char c : s.toCharArray() )
        {
            if(Character.isWhitespace( c ))
            {
                lastSpace = true;
            }
            else
            {
                /** Increase the word counter when the last character is a space and the
                 * currently traversed one isn't
                 */
                if (lastSpace)
                    count++;
                lastSpace = false;
            }
        }

        return count;
    }

    private static int countWords(Stream<Character> stream)
    {
        WordCount wordCounter = stream.reduce(new WordCount(0, true), WordCount::accumluate, WordCount::combine);
        return wordCounter.getCounter();
    }
}
