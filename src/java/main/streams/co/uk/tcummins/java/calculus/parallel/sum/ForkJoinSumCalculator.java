package co.uk.tcummins.java.calculus.parallel.sum;

/**
 * Created by tcummins on 03.03.16.
 * Project : JavaPlay
 *
 * Extend RecursiveTask to create a task usable with the fork/join framework
 */
public class ForkJoinSumCalculator extends java.util.concurrent.RecursiveTask<Long>
{
    /** The array of numbers to be summed */
    private final long[] numbers;

    /** The initial and final position of the portion of the array processed by this subtask */
    private final int start;
    private final int end;

    /** The size of the array under which this task is no longer split into subtasks */
    public static final long THRESHOLD = 10_000;


    /**
     * Public constructor used to create the main task
     *
     * @param numbers
     */
    public ForkJoinSumCalculator( final long[] numbers )
    {
        this(numbers, 0, numbers.length);
    }


    /**
     * Private constructor used to recursively create subtasks of the main task
     *
     * @param numbers
     * @param start
     * @param end
     */
    private ForkJoinSumCalculator( final long[] numbers, final int start, final int end )
    {
        this.numbers = numbers;
        this.start = start;
        this.end = end;
    }


    /**
     * Override the abstract method of RecursiveTask
     *
     * @return
     */
    @Override
    protected Long compute()
    {
        /** The size of the portion of the array summed by this task */
        int length = end - start;

        if( length <= THRESHOLD )
        {
            /** If the size is less than or equal to the threshold, compute the result sequentially */
            return computeSequentially();
        }

        /** Create a subtask to sum the first hald of the array */
        ForkJoinSumCalculator leftTask = new ForkJoinSumCalculator( numbers, start, start + length/2 );
        /** Asynchronously execute the newly created subtask using another thread of the ForkJoinPool */
        leftTask.fork();

        /** Create a subtask to sum the second half of the array */
        ForkJoinSumCalculator rightTask = new ForkJoinSumCalculator( numbers, start + length/2, end );

        /** Execute this second subtask synchronously, potentially allowing further recursive splits */
        Long rightResult = rightTask.compute();
        /** Read the result of the first subtask, or wait for it if isn't ready */
        Long leftResult = leftTask.compute();

        /** The result of this task the combination of the results of the two subtasks */
        return leftResult + rightResult;
    }


    /**
     * Simple algorithm calculating the result of a subtask when it's no longer divisible
     *
     * @return
     */
    private long computeSequentially()
    {
        long sum = 0;

        for ( int i = 0; i < end ; i++ )
        {
            sum += numbers[i];
        }

        return sum;
    }
}