package co.uk.tcummins.java.calculus.prime.number;

import static java.util.stream.Collectors.partitioningBy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;


/**
 * Created by tcummins on 02.03.16.
 * Project : JavaPlay
 */
public class CollectorHarness
{
    /**
     * partitionPrimes = Quoted CPU Time    : 4716 msecs
     *                 = Work CPU Time      : 513 msecs
     *                 = Laptop CPU Time    : 922 msecs
     *
     * partitionPrimesWithCustomCollector = Quoted CPU Time     : 3201 msecs
     *                                    = Work CPU Time       : 19054 msecs
     *                                    = Laptop CPU Time     : 31389 msecs
     * @param args
     */
    public static void main( String[] args )
    {
        long fastest = Long.MAX_VALUE;

        for ( int i = 0; i < 1; i++ )
        {
            long start = System.nanoTime();

            //partitionPrimes( 1_000_000 );

            partitionPrimesWithCustomCollector( 1_000_000 );

            long duration = (System.nanoTime() - start) / 1_000_000;

            if( duration < fastest )
                fastest = duration;
        }

        System.out.println( "Fastest execution done in " + fastest + " msecs" );
    }


    private static Map<Boolean, List<Integer>> partitionPrimesWithCustomCollector( int n )
    {
        return IntStream.rangeClosed( 2, n ).boxed().collect( new PrimeNumberCollector() );

//        return IntStream.rangeClosed( 2, n ).boxed().collect( () -> new HashMap<Boolean, List<Integer>>()
//        {{
//            put( true, new ArrayList<Integer>(  ) );
//            put( false, new ArrayList<Integer>(  ) );
//        }},
//        (acc, candidate ) ->
//        {
//            acc.get( isPrime( acc.get(true), candidate )).add(candidate);
//        },
//        (map1, map2) ->
//        {
//            map1.get(true).addAll(map2.get(true));
//            map1.get(false).addAll(map2.get(true));
//        });
    }


    private static Map<Boolean, List<Integer>> partitionPrimes( int n )
    {
        return IntStream.rangeClosed( 2, n ).boxed().collect( partitioningBy( CollectorHarness::isPrime ) );
    }


    private static boolean isPrime( int candidate )
    {
        int candidateRoot = (int) Math.sqrt( (double) candidate );
        return IntStream.rangeClosed( 2, candidateRoot ).noneMatch( i -> candidate % i == 0 );
    }


    public static boolean isPrime(List<Integer> primes, int candidate)
    {
        return primes.stream().noneMatch(i -> candidate % i == 0);
    }
}