package co.uk.tcummins.java.dish;

import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;

import static java.util.stream.Collector.Characteristics.*;


/**
 * Created by tcummins on 02.03.16.
 * Project : JavaPlay
 */
public class ToListCollector<T> implements Collector<T, List<T>, List<T>>
{
    /**
     * Creates the collection operation starting point
     * 
     * @return
     */
    @Override
    public Supplier<List<T>> supplier()
    {
        return ArrayList::new;
    }


    /**
     * Accumulates the traversed item, modifying the accumulator in place
     * 
     * @return
     */
    @Override
    public BiConsumer<List<T>, T> accumulator()
    {
        return List::add;
    }


    /**
     *
     * @return Identify function
     */
    @Override
    public Function<List<T>, List<T>> finisher()
    {
        return Function.identity();
    }


    /**
     * Modifies the first accumulator combinding it with the content of the second one
     *
     * @return The modified first accumulator
     */
    @Override
    public BinaryOperator<List<T>> combiner()
    {
        return ( list1, list2 ) ->
        {
            list1.addAll( list2 );
            return list1;
        };
    }


    @Override
    public Set<Characteristics> characteristics()
    {
        return Collections.unmodifiableSet( EnumSet.of( IDENTITY_FINISH, CONCURRENT ) );
    }
}