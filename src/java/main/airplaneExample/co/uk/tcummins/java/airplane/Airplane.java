package co.uk.tcummins.java.airplane;

public final class Airplane implements Runnable
{
    public Airplane (Airport aAirport, String aFlightId)
    {
        fAirport = aAirport;
        fFlightId = aFlightId;
    }

    @Override public void run()
    {
        takeOff();
        fly();
        land();
    }

    private Airport fAirport;
    private String fFlightId;

    private void takeOff()
    {
        synchronized( fAirport )
        {
            //always use a while loop, never an if-statement:
            while (!fAirport.hasAvailableRunway())
            {
                System.out.println( fFlightId + ": waiting for runway..." );
                try
                {
                    //wait for notification from the airport that
                    //the state of the airport has changed.
                    //wait must always occur within a synchronized block
                    fAirport.wait();
                }
                catch (InterruptedException ex)
                {
                    Thread.currentThread().interrupt();
                    System.err.println( ex );
                }
            }
            //there is an available runway now, so we may take off
            System.out.println(fFlightId + ": taking off now...");
        }
    }

    private void fly()
    {
        System.out.println(fFlightId + ": flying now...");
        try
        {
            //do nothing for several seconds
            Thread.sleep(10000);
        }
        catch (InterruptedException ex)
        {
            System.err.println(ex);
            Thread.currentThread().interrupt();
        }
    }

    private void land()
    {
        synchronized(fAirport)
        {
            while (!fAirport.hasAvailableRunway())
            {
                //wait for notification from the airport that
                //the state of the airport has changed.
                System.out.println(fFlightId + ": waiting for runway...");
                try
                {
                    fAirport.wait();
                }
                catch (InterruptedException ex)
                {
                    System.err.println( ex );
                    Thread.currentThread().interrupt();
                }
            }
            //there is an available runway now, so we may take off
            System.out.println(fFlightId + ": landing now...");
        }
    }
}