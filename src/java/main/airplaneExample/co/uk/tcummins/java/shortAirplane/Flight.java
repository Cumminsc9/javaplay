package co.uk.tcummins.java.shortAirplane;

public class Flight implements Comparable<Flight>
{
    private String fName;


    public Flight( String n )
    {
        fName = n;
    }


    public String getName()
    {
        return fName;
    }


    public int compareTo( Flight f )
    {
        return this.getName().compareTo( f.getName() );
    }


    public static void main( String[] args )
    {
        // create your Runways
        Runway r1 = new Runway( "Runway 1" );
        Runway r2 = new Runway( "Runway 2" );

        // create your Threads
        Thread t1 = new Thread( r1 );
        Thread t2 = new Thread( r2 );

        // create your Flight and pass it to Runway 1
        Flight f1 = new Flight( "Flight 1" );
        Flight f2 = new Flight( "Flight 2" );
        r1.add( f1 );
        r1.add( f2 );

        // create more Flight and pass it to Runway 2
        Flight f3 = new Flight( "Flight 3" );
        Flight f4 = new Flight( "Flight 4" );
        Flight f5 = new Flight( "Flight 5" );
        r2.add( f3 );
        r2.add( f4 );
        r2.add( f5 );

        // start your airport simulation and run it for 6 seconds
        t2.start();
        t1.start();
        try
        {
            Thread.currentThread().sleep( 6000 );
        }
        catch( InterruptedException e )
        {
        }

        // interrrupt the threads, because after 6 seconds all flights
        // should be landeded

        t1.interrupt();
        t2.interrupt();
    }

}