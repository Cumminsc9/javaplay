package co.uk.tcummins.java.shortAirplane;

import java.util.PriorityQueue;
import java.util.Queue;

public class Runway implements Runnable
{

    Queue<Flight> q;
    String name;
    private final Object syncObject = new Object();


    public Runway( String n )
    {
        q = new PriorityQueue<>();
        name = n;
        System.out.println( n + " ready." );
    }


    public void add( Flight f )
    {
        q.add( f );
    }


    public String getRunway()
    {
        return name;
    }


    public void run()
    {
        synchronized( syncObject )
        {
            while( !Thread.currentThread().isInterrupted() )
            {
                if( q.size() > 0 )
                {
                    Flight f = q.peek();
                    System.out.println( this.getRunway() + ": " + f.getName() + " is about to land" );
                    try
                    {
                        Thread.currentThread();
                        Thread.sleep( 1000 );
                    }
                    catch( InterruptedException e )
                    {
                        Thread.currentThread().interrupt();
                    }
                    System.out.println( this.getRunway() + ": " + f.getName() + " landed." );
                    q.remove( f );
                }
            }
        }
    }
}