package co.uk.tcummins.java.scheduledThreadPool;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;


/**
 * @author Chandan Singh
 * See :: http://examples.javacodegeeks.com/core-java/util/concurrent/scheduledthreadpoolexecutor/java-util-concurrent-scheduledthreadpoolexecutor-example/
 *
 */
public class ScheduledThreadPoolExecutorExample
{
    public static void main(String[] args) throws InterruptedException, ExecutionException
    {

        Runnable runnabledelayedTask = new Runnable()
        {
            @Override
            public void run()
            {
                try
                {
                    System.out.println(Thread.currentThread().getName()+" is Running Delayed Task");
                    throw new RuntimeException( "bang!" );
                }
                catch ( Throwable t )
                {
                    t.printStackTrace();
                }
            }
        };


        Callable callabledelayedTask = new Callable()
        {

            @Override
            public String call() throws Exception
            {
                return "GoodBye! See you at another invocation...";
            }
        };

        ScheduledExecutorService scheduledPool = Executors.newScheduledThreadPool(4);

        scheduledPool.scheduleWithFixedDelay(runnabledelayedTask, 1, 1, TimeUnit.SECONDS);

        ScheduledFuture sf = scheduledPool.schedule(callabledelayedTask, 4, TimeUnit.SECONDS);

        String value = String.valueOf( sf.get() );

        System.out.println("Callable returned"+value);

        scheduledPool.shutdown();

        System.out.println("Is ScheduledThreadPool shutting down? "+scheduledPool.isShutdown());
    }
}