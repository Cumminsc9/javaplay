package co.uk.tcummins.java.javasemaphore;

import java.util.concurrent.Semaphore;

public class JavaSemaphoreTut
{
    private static final int MAX_CON_THREADS = 2;
    private final Semaphore adminLock = new Semaphore( MAX_CON_THREADS, true );

    public static void main(String[] args)
    {
        JavaSemaphoreTut test = new JavaSemaphoreTut();
        test.startTest();
    }

    public void startTest()
    {
        for ( int i = 0; i < 2; i++ )
        {
            Person p = new Person();
            p.start();
        }
    }

    public class Person extends Thread
    {
        public void run()
        {
            try
            {
                //Acquire lock
                adminLock.acquire();
            }
            catch ( InterruptedException e )
            {
                System.out.println();
                return;
            }

            System.out.println( "Thread " + this.getId() + " starts using admin's car - Acquire()" );
            try
            {
                sleep( 1000 );
            }
            catch ( Exception ignored )
            {

            }
            finally
            {
                // Release Lock
                adminLock.release();
            }
            System.out.println( "Thread " + this.getId() + " stops using admin's car -  Release()\n" );
        }
    }
}