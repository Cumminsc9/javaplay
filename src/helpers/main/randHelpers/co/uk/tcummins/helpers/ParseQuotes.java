package co.uk.tcummins.helpers;

import static java.util.regex.Pattern.compile;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Pass and print results like :: ParseQuotes.parseStringQuotes( string ).forEach( System.out::println );
 */
public class ParseQuotes
{
    private final static Pattern pattern = compile( "\"(.+?)\"" ); // OR ("\"([^\"]+)\"")
    private static ArrayList<String> strings = new ArrayList<>(  );

    public static List<String> parseStringQuotes( final String string )
    {
        Matcher matcher = pattern.matcher( string );

        while( matcher.find() )
        {
            strings.add( matcher.group( 1 ) );
        }

        return strings;
    }
}