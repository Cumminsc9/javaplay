package co.uk.tcummins.helpers;


/**
 * Created by tcummins on 17.05.16.
 */
public class ConvertSecondsToDays
{
    private static final long ONE_MINUTE = 60000;
    private static final long TEN_MINUTES = 10 * 60 * 1000L;
    private static final long ONE_HOUR = 60 * 60 * 1000L;
    private static final long THREE_HOURS = 3 * ONE_HOUR;
    private static final long SIX_HOURS = 6 * ONE_HOUR;
    private static final long HALF_DAY = 12 * ONE_HOUR;
    private static final long ONE_DAY = 24 * ONE_HOUR;
    private static final long TWO_DAYS = 2 * ONE_DAY;

    public static void main( String[] args )
    {
        // Without grace period
        Object actual_expiry = 1471017600;
        String actualPeroidLength = actual_expiry + "000";


        // Grace period
        Object grace_period = 1209600;
        String gracePeriodLength = grace_period + "000";


        // With grace period
        Object expiry_date = 1472227200;
        String expiryPeriodLength = expiry_date + "000";


        long foo = Long.parseLong( expiryPeriodLength ) - Long.parseLong( gracePeriodLength );
        System.out.println( foo );


//        long calc = Long.parseLong( expiryPeriodLength ) - Long.parseLong( actualPeroidLength );
//        System.out.println( calc );
    }
}