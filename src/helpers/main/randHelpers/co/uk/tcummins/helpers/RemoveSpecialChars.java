package co.uk.tcummins.helpers;

/**
 * Created by tcummins on 15.03.16.
 * Project : JavaPlay
 */
public final class RemoveSpecialChars
{
    /**
     * Will remove all special characters from the passed in String ( except spaces )
     *
     * @param str The String to be parsed
     * @return The parsed String
     */
    public static String parseSpecialChars( final String str )
    {
        return str.replaceAll( "[^A-Za-z0-9 ]", "" );
    }
}