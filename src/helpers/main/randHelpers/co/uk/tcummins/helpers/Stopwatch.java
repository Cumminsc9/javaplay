package co.uk.tcummins.helpers;

/**
 *  Simple Stopwatch class
 */
public final class Stopwatch
{
    private static long startTime = 0;
    private static long stopTime = 0;
    private static boolean running = false;


    /**
     * Starts the stopwatch
     */
    public static void start()
    {
        startTime = System.currentTimeMillis();
        running = true;
    }


    /**
     * Stops the stopwatch
     */
    public static void stop()
    {
        stopTime = System.currentTimeMillis();
        running = false;
    }


    /**
     * Gets elapsed time.
     *
     * @return the elapsed time in milliseconds
     */
    public static long getElapsedTime()
    {
        long elapsed;
        if( running )
        {
            elapsed = (System.currentTimeMillis() - startTime);
        }
        else
        {
            elapsed = (stopTime - startTime);
        }
        return elapsed;
    }


    /**
     * Gets elapsed time secs.
     *
     * @return the elapsed time in seconds
     */
    public static long getElapsedTimeSecs()
    {
        long elapsed;
        if( running )
        {
            elapsed = ((System.currentTimeMillis() - startTime) / 1000);
        }
        else
        {
            elapsed = ((stopTime - startTime) / 1000);
        }
        return elapsed;
    }
}