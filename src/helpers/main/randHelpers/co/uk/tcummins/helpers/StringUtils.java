package co.uk.tcummins.helpers;

import java.util.StringTokenizer;

public class StringUtils
{
    public static final String DOUBLE_QUOTE = "\"";
    public static final String ESCAPED_DOUBLE_QUOTE = "\"\"";

    public static String ToTitleCase( final String str )
    {
        if( str == null || str.isEmpty() )
        {
            return str;
        }
        final StringBuilder rv = new StringBuilder();
        final StringTokenizer strtok = new StringTokenizer( str );
        while( strtok.hasMoreTokens() )
        {
            final String word = strtok.nextToken();
            final String firstLetter = word.substring( 0, 1 );
            final String restOfWord = word.substring( 1 );
            rv.append( firstLetter.toUpperCase() ).append( restOfWord.toLowerCase() );
        }
        return rv.toString();
    }


    public static String toLogString( String string )
    {
        if( string != null && string.length() > 0 )
        {

            if( string.indexOf( " " ) > 0 )
            {
                return string.replace( ' ', '+' );
            }
            else
            {
                return string;
            }
        }

        return ESCAPED_DOUBLE_QUOTE;
    }


    public static String toQuotedString( final String string )
    {
        if( string != null && string.length() > 0 )
        {
            return "\"" + escapeQuotes( string ) + "\"";
        }

        return "\"\"";
    }


    public static String toLogQuotedString( String string )
    {
        if( string != null && string.length() > 0 )
        {
            if( string.contains( DOUBLE_QUOTE ) )
            {
                return DOUBLE_QUOTE + escapeQuotes( string ) + DOUBLE_QUOTE;
            }
            else
            {
                return toLogString( string );
            }
        }

        return ESCAPED_DOUBLE_QUOTE;
    }


    public static String escapeQuotes( String s )
    {
        if( s == null )
        {
            return null;
        }

        final int sLength = s.length();

        StringBuffer sb = null;

        int c = 0; // count the number of replacements to optimize memory usage
        int i = 0;
        int x = 0;

        while( (i = s.indexOf( DOUBLE_QUOTE, i )) > -1 )
        {
            i++;
            c++;
        }

        if( c == 0 )
        {
            return s;
        }

        sb = new StringBuffer( sLength + c );

        int j = 0;

        while( (x = s.indexOf( DOUBLE_QUOTE, j )) > -1 )
        {
            sb.append( s.substring( j, x ) );
            sb.append( ESCAPED_DOUBLE_QUOTE );
            j = x + 1;
        }

        sb.append( s.substring( j ) );

        return sb.toString();
    }
}
