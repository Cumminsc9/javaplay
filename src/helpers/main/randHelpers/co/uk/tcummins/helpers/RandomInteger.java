package co.uk.tcummins.helpers;

import java.util.Random;

public class RandomInteger
{
    /**
     * Returns a pseudo-random number between min and max, inclusive.
     * The difference between min and max can be at most
     * <code>Integer.MAX_VALUE - 1</code>.
     *
     * @param min Minimum value
     * @param max Maximum value.  Must be greater than min.
     * @return Integer between min and max, inclusive.
     * @see java.util.Random#nextInt(int)
     * http://stackoverflow.com/questions/363681/generating-random-integers-in-a-range-with-java/363692#363692
     */
    public static int randomInt( int min, int max )
    {
        Random rand = new Random();

        // nextInt is normally exclusive of the top value,
        // so add 1 to make it inclusive
        return rand.nextInt((max - min) + 1) + min;
    }
}

// In particular, there is no need to reinvent the random integer generation wheel
// when there is a straightforward API within the standard library to accomplish the task.

// In Java 1.7 or later, the following method is even more straightforward as long
// as there is no need to explicitly set the initial seed:

/*
    import play.util.concurrent.ThreadLocalRandom;
    ThreadLocalRandom.current().nextInt(min, max + 1);
*/