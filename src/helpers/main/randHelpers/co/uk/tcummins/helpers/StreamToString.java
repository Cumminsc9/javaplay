package co.uk.tcummins.helpers;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;


public class StreamToString
{
    private static final String UTF8 = "utf-8";

    /**
     * Will default to utf-8 encoding
     * @see #streamToString(java.io.InputStream, String)
     *
     * @since 6.0.0
     */
    public static String streamToString( final InputStream stream ) throws IOException
    {
        return streamToString( stream, UTF8 );
    }

    /**
     * This method will convert an {@link java.io.InputStream} into a {@link java.lang.String}
     *
     * @param stream the {@link java.io.InputStream} to convert
     * @param encoding The name of a supported
     *         {@link java.nio.charset.Charset charset}
     *
     * @return the stream converted into a string
     *
     * @throws IOException
     *
     * @since 6.0.0
     */
    private static String streamToString( final InputStream stream, final String encoding ) throws IOException
    {
        final char[] buffer = new char[4096];
        final StringBuilder out = new StringBuilder();
        final Reader in = new InputStreamReader( stream, encoding );
        try
        {
            for( ;; )
            {
                int rsz = in.read( buffer );
                if( rsz < 0 )
                    break;
                out.append( buffer, 0, rsz );
            }
        }
        finally
        {
            in.close();
        }
        return out.toString();
    }
}
