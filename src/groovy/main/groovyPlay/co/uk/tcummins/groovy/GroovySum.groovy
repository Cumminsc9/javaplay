package co.uk.tcummins.groovy

/**
 * Created by tcummins on 20.11.15.
 */
class GroovySum
{
    public static void main(String[] args)
    {
        Scanner scanner = new Scanner(System.in)

        print "To quit enter '\\q'\n"
        print "For JavaSum enter '1' for GroovySum1 enter '2' for GroovySum2 enter '3' \nAwaiting input: "
        def read = scanner.next()

        switch (read)
        {
            case 1.toString(): JavaSum1()
                break;
            case 2.toString(): GroovySum2()
                break;
            case 3.toString(): GroovySum3()
                break;
            default: println('No good value entered')
                break;
        }
    }

    def static JavaSum1()
    {
        System.out.println('Enter numbers to sum, separated by a space: ');
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String line = "";
        try
        {
            line = br.readLine();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        String[] inputs = line.split(" ");
        double total = 0.0;
        for(String s :inputs)
        {
            total += Double.parseDouble(s);
        }
        System.out.println("The sum is: " + total);
    }

    def static GroovySum2()
    {
        println 'Enter numbers to sum, separated by a space: '
        System.in.withReader
                {
                    br -> println br.readLine().tokenize()*.toBigDecimal().sum()
                }
    }

    def static GroovySum3() {
        println 'Enter numbers to sum, separated by a space (Sums numbers with looping): '
        System.in.eachLine
                {
                    line ->
                        if (!line) System.exit(0)
                        println line.split(' ')*.toBigDecimal().sum()
                }
    }
}
