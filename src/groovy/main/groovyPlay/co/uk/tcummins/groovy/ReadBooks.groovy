package co.uk.tcummins.groovy

import org.w3c.dom.NodeList

import javax.lang.model.element.Element
import javax.xml.parsers.DocumentBuilder
import javax.xml.parsers.DocumentBuilderFactory

/**
 * Created by tcummins on 20.11.15.
 */
class ReadBooks
{
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in)

        print"For readXML0 enter '0' for readXML1 enter '1' \nAwaiting input: "
        def read = scanner.next()
        switch (read)
        {
            case 0.toString(): readXML0()
                break;
            case 1.toString(): readXML1()
                break;
            default: println('No good value entered')
                break;
        }
    }

    def static readXML0()
    {
        def root = new XmlSlurper().parse("src/resources/books.XML")
        println "The title is: " + root.book[1].title
    }

    def static readXML1()
    {
        DocumentBuilderFactory fact = DocumentBuilderFactory.newInstance()
        def doc = null

        try
        {
            DocumentBuilder builder = fact.newDocumentBuilder()
            doc = builder.parse("src/resources/books.XML")
        }
        catch (Exception e)
        {
            e.printStackTrace()
        }

        if (doc == null) return

        NodeList titles = doc.getElementsByTagName("title")
        Element titleNode = titles.item(0) as Element
        String title = titleNode.getFirstChild().getNodeValue()
        println "The title is: $title"
    }
}
