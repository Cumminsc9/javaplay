package co.uk.tcummins.groovy

/**
 * Created by tcummins on 20.11.15.
 */
class EncodeDecode
{
    public static void main(String[] args)
    {
        def u = 'username'
        def p = 'password'

        def encodeAll  = "$u:$p".getBytes().encodeBase64().toString()
        def encodeUser = "$u".getBytes().encodeBase64().toString()
        def encodePass = "$p".getBytes().encodeBase64().toString()

        println("$u -> $encodeUser")
        println("$p -> $encodePass")

        def (decodeU, decodeP) = new String(encodeAll.decodeBase64()).split(':')
        def (user) = new String(encodeUser.decodeBase64()).split(':')
        def (pass) = new String(encodePass.decodeBase64()).split(':')

        println("username -> $user")
        println("password -> $pass")

        println("$u:$p -> $encodeAll")
        println("username:password -> $decodeU:$decodeP")

        assert user == u
        assert pass == p
    }
}
