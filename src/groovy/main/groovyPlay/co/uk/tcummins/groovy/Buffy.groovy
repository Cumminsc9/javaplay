package co.uk.tcummins.groovy

/**
 * Created by tcummins on 20.11.15.
 */

class Buffy
{
    public static void main(String[] args)
    {
        def buffy = new Char(name: 'Buffy', id: 0)
        assert buffy.id == 0
        assert buffy.name == 'Buffy'

        def faith = new Char(name: 'Faith', id: 1)
        assert faith.id == 1
        assert faith.name == 'Faith'

        def willow = new Char(name: 'Willow', id: 2) as Char
        assert willow.getId() == 2
        assert willow.getName() == 'Willow'

        def slayers = [buffy, faith]
        assert ['Buffy','Faith'] == slayers*.name
        assert slayers.class == ArrayList

        def chars = slayers + willow
        assert ['Buffy','Faith','Willow'] == chars*.name

        def doubles = chars.findAll{ it.name =~ /([a-z])\1/}
        assert ['Buffy','Willow'] == doubles*.name

        println(buffy.toString())
        println(faith.toString())
        println(willow.toString())

        println('slayers: ' + slayers)
        println('chars: ' + chars)
        println('doubles: ' + chars)
    }
}

class Char
{
    def id
    def name

    def getId() {
        return id
    }

    void setId(final id) {
        this.id = id
    }

    def getName() {
        return name;
    }

    void setName(final name) {
        this.name = name
    }

    @Override
    String toString() {
        return "Char" + "id=" + id + ", name=" + name + '';
    }
}
