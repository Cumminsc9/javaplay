package co.uk.tcummins.groovy

import groovy.json.JsonSlurper

/**
 * Created by tcummins on 20.11.15.
 */
class ChuckNorris
{
    public static void main(String[] args)
    {
        String url = 'http://api.icndb.com/jokes/random?limitTo=[nerdy]'
        String jsonTxt = url.toURL().text

        def json = new JsonSlurper().parseText(jsonTxt)
        def joke = json?.value?.joke

        println("$joke")
    }
}
