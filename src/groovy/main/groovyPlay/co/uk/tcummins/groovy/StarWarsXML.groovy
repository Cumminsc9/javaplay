package co.uk.tcummins.groovy

import groovy.xml.MarkupBuilder

/**
 * Created by tcummins on 20.11.15.
 */
class StarWarsXML
{
    public static void main(String[] args)
    {
        def fw = new FileWriter("src/resources/StarWars.XML")

        def builder = new MarkupBuilder(fw)

        builder.StarWars
                {
                    title "Star Wars: Episode VII - The Force Awakens"
                    description "A continuation of the saga created by George Lucas and set thirty years after " +
                            "Star Wars: Episode VI - Return of the Jedi (1983)."
                    imdb_link "http://www.imdb.com/title/tt2488496/"
                    release "18 December 2015"
                    runtime "136 Minutes"
                    director "J.J. Abramns"

                    best_triva "An article in the June 2015 edition of Vanity Fair states that J.J. Abrams has an idea to place " +
                            "Jar Jar Binks's skeleton (as an Easter egg) into a few seconds of the film. This would be set in a desert."

                    builder.production_companies
                            {
                                name "LucasFilm"
                                name "Bad Robot Productions"

                                builder.distributed_by
                                        {
                                            name "Walt Disney Studios Motion Pictures"
                                        }
                            }

                    builder.writers
                            {
                                name "Lawrence Kasdan"
                                name "J.J. Abrams"
                                name "Michael Arndt"
                                name "George Lucas"
                            }

                    builder.cast
                            {
                                name "Mark Hamill"
                                builder.alias
                                        {
                                            name "Luke Skywalker"
                                        }
                                name "Harrison Ford"
                                builder.alias
                                        {
                                            name "Han Solo"
                                        }
                                name "Carrie Fisher"
                                builder.alias
                                        {
                                            name "Leia Solo"
                                        }
                                name "Dasiy Ridley"
                                builder.alias
                                        {
                                            name "Rey"
                                        }
                                name "John Boyega"
                                builder.alias
                                        {
                                            name "Finn"
                                        }
                                name "Adam Driver"
                                builder.alias
                                        {
                                            name "Kylo Ren"
                                        }
                                name "Kenny Baker"
                                builder.alias
                                        {
                                            name "R2-D2"
                                        }
                                name "Peter Mayhew"
                                builder.alias
                                        {
                                            name "Chewbacca"
                                        }
                                name "Anthony Daniels"
                                builder.alias
                                        {
                                            name "C-3PO"
                                        }
                            }
                }

        def root = new XmlSlurper().parse("src/resources/StarWars.XML")
        println root.children()
    }
}