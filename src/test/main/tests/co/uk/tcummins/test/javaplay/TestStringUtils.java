package co.uk.tcummins.test.javaplay;

import co.uk.tcummins.helpers.StringUtils;
import org.junit.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;

/**
 * Created by tcummins on 15.03.16.
 * Project : JavaPlay
 */
public class TestStringUtils
{

    public TestStringUtils()
    {
    }


    @Test
    public void testToTitleCase()
    {
        assertEquals( "Tom", StringUtils.ToTitleCase( "tOM" ) );
        assertEquals( "Cummins", StringUtils.ToTitleCase( "cumMINS" ) );
        assertEquals( "Noop", StringUtils.ToTitleCase( "Noop" ) );
        assertEquals( "Abcd", StringUtils.ToTitleCase( "aBcD" ) );
        assertEquals( "", StringUtils.ToTitleCase( "" ) );
        assertNull( StringUtils.ToTitleCase( null ) );
    }


    @Test
    public void testToLogString() throws Exception
    {
        assertEquals("\"\"", StringUtils.toLogString( "" ));
        assertEquals("this", StringUtils.toLogString( "this" ));
        assertEquals("this+is", StringUtils.toLogString( "this is" ));
        assertEquals("this+is+a", StringUtils.toLogString( "this is a" ));
        assertEquals("this+is+a+test", StringUtils.toLogString( "this is a test" ));

        assertEquals("\"\"", StringUtils.toLogString( "" ));
        assertEquals("\"this\"", StringUtils.toLogString( "\"this\"" ));
        assertEquals("\"this+is\"", StringUtils.toLogString( "\"this is\"" ));
        assertEquals("\"this+is+a\"", StringUtils.toLogString( "\"this is a\"" ));
        assertEquals("\"this+is+a+test\"", StringUtils.toLogString( "\"this is a test\"" ));
    }


    @Test
    public void testToLogQuotedString() throws Exception
    {
        assertEquals("\"\"", StringUtils.toLogQuotedString( "" ));
        assertEquals("this", StringUtils.toLogQuotedString( "this" ));
        assertEquals("this+is", StringUtils.toLogQuotedString( "this is" ));
        assertEquals("this+is+a", StringUtils.toLogQuotedString( "this is a" ));
        assertEquals("this+is+a+test", StringUtils.toLogQuotedString( "this is a test" ));

        assertEquals("\"\"", StringUtils.toLogQuotedString( "" ));
        assertEquals("\"\"\"this\"\"\"", StringUtils.toLogQuotedString( "\"this\"" ));
        assertEquals("\"\"\"this is\"\"\"", StringUtils.toLogQuotedString( "\"this is\"" ));
        assertEquals("\"\"\"this is a\"\"\"", StringUtils.toLogQuotedString( "\"this is a\"" ));
        assertEquals("\"\"\"this is a test\"\"\"", StringUtils.toLogQuotedString( "\"this is a test\"" ));
    }


    @Test
    public void testEscapeQuotes() throws Exception
    {
        assertEquals("", StringUtils.escapeQuotes( "" ));
        assertEquals("this", StringUtils.escapeQuotes( "this" ));
        assertEquals("this is", StringUtils.escapeQuotes( "this is" ));
        assertEquals("this is a", StringUtils.escapeQuotes( "this is a" ));
        assertEquals("this is a test", StringUtils.escapeQuotes( "this is a test" ));

        assertEquals("\"\"this\"\"", StringUtils.escapeQuotes( "\"this\"" ));
        assertEquals("\"\"this is\"\"", StringUtils.escapeQuotes( "\"this is\"" ));
        assertEquals("\"\"this is a\"\"", StringUtils.escapeQuotes( "\"this is a\"" ));
        assertEquals("\"\"this is a test\"\"", StringUtils.escapeQuotes( "\"this is a test\"" ));
    }
}